This directory contains scripts and files used for continuous delivery.

We use docker containers to deliver because of its simplicity.
Moreover, such containers can be locally built to debug integration issues
without the real infrastructure.

A [makefile][] is provided for convenience but please take notice of the
requirements of each image.
Make sure that the docker daemon is running on your computer before running
make.
To build all targets, simply run `make`.
To build a specific image, run `make <image>`;
By convention, images are built from the dockerfile named `dockerfile-<image>`.

[makefile]: https://opensource.com/article/18/8/what-how-makefile

## flutter-app

### Requirements

Go into the `flutter-app` directory and follow the release instructions in
the README file.

### Execution

This image is based on `nginx` and serves a bunch of static files on port 80.
The files are copied from `../flutter-app/build/web/` and will be accessible.
The image can be run using the following command:

```shell
$ docker run --rm -p 8000:80 flutter-app
```

The web application will be accessible under http://localhost:8000/
