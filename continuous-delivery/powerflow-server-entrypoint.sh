#!/bin/sh

die()
{
    retval="$1"; shift
    if [ $# -eq 0 ]; then # Message is in stdin.
        cat <&0 >&2
    else  # Message is an argument.
        printf "%s" "$@" >&2; echo >&2
    fi
    exit "$retval"
}

SERVER_DB_URL="${SERVER_DB_URL:-$(cat /run/secrets/server_db_url)}"
SERVER_DB_USER="${SERVER_DB_USER:-$(cat /run/secrets/server_db_user)}"
SERVER_DB_PASS="${SERVER_DB_PASS:-$(cat /run/secrets/server_db_pass)}"

[ -n "$SERVER_DB_URL" ] || die 1 "Missing server_db_url secret!"
[ -n "$SERVER_DB_USER" ] || die 1 "Missing server_db_user secret!"
[ -n "$SERVER_DB_PASS" ] || die 1 "Missing server_db_pass secret!"

echo "Starting server application using database $SERVER_DB_URL."

exec java -jar /app.jar \
  --db.url="$SERVER_DB_URL" \
  --db.user="$SERVER_DB_USER" \
  --db.pass="$SERVER_DB_PASS"
