# Powerflow infrastructure components

This directory contains files used on the production deployment, which have
otherwise not been automated as part of the continuous deployment (CD) strategy.

Most of these files support the CD strategy and are not expected to
**continuously** evolve over time.

## Continuous delivery strategy

The diagram bellow describes the automation strategy to provide continuous
delivery: The CI will trigger a webhook on the production server.
This request will be received by traefik, which is running in the production
deployment using docker-compose.
The request will pass through an authentication middleware, which validates
the principal can call the specified endpoint;
if the principal is authenticated and authorized to access the requested
webhook the request will be proxied to the [webhook][] service running on the
container host.

```mermaid
graph LR
  subgraph CI["Continuous integration"]
    subgraph ToStaging["Any branch (manual action)"]
      InvokeStagingDeploy["Deploy to staging"]
      InvokeStagingCheck["Check staging deployment"]
    end
    subgraph ToProd["'main' branch (upon merge)"]
      InvokeProdDeploy["Deploy to production"]
      InvokeProdCheck["Check production deployment"]
    end
  end
  subgraph Server["Powerflow server"]
    subgraph StagingHooks["webhooks for Staging deployment"]
      StagingDeploy["Update images from branch and deploy staging"]
      StagingCheck["Check whether deployment to staging was successful"]
    end
    subgraph Prod["Production deployment"]
      subgraph Traefik
        AuthForStaging["Authentication middleware for staging webhooks"]
        AuthForProd["Authentication middleware for production webhooks"]
      end
      subgraph ProdHooks["webhooks for Production deployment"]
        ProdDeploy["Update images from 'latest' tag and deploy production"]
        ProdCheck["Check whether deployment to production was successful"]
      end
    end

    AuthForStaging --> StagingDeploy
    AuthForStaging --> StagingCheck

    AuthForProd --> ProdDeploy
    AuthForProd --> ProdCheck
  end

  InvokeStagingDeploy -->|staging auth| AuthForStaging
  InvokeStagingCheck -->|staging auth| AuthForStaging

  InvokeProdDeploy -->|production auth| AuthForProd
  InvokeProdCheck -->|production auth| AuthForProd
```

## Expected configuration

The server is expected to be publicly reachable on ports 80 and 443.
This is required to succeed the ACME challenge and obtain an SSL certificate
from [let's encrypt][le].

[le]: https://letsencrypt.org/

Port 9000 should also be accessible to the containers via
`host.docker.internal` where the [webhook][] service should run.
The docker-compose is configured to create a
[docker compose network][compose-network] through interface `br-pf`, which may
be used to anchor firewall rules.

[compose-network]: https://docs.docker.com/compose/networking/#specify-custom-networks

## Useful information about the server

The server runs in a VM provided by the IICT department at HEIG-VD.
The VM is running [Ubuntu 20.04.3 LTS (focal)][ubuntu-focal].
We use `ufw` to configure firewal (This may change to `iptables` in the future).

[ubuntu-focal]: http://releases.ubuntu.com/focal/

Current docker version: `20.10.7-0ubuntu5~20.04.2` (from `focal-updates`).
Current docker-compose version: `1.25.0-1` (from `focal-universe`).

### Useful commands

- fw: check configuration: `sudo ufw status`.
- fw: allow access to port 443: `sudo ufw allow 443/tcp`.
- fw: allow accessing the webhook service from the container network:
  `sudo ufw allow in on br-pf to any port 9000`.
- docker: Open shell in the `traeffik` container:
  `docker exec -it $(docker ps | grep traefik: | awk '{print $1'}) /bin/sh`.

## Secrets

A `secrets-staging` and `secrets-production` directory is expected to be
present containing the following files:

- server_db_user
- server_db_pass

## Files

### docker-compose.yml

A [docker-compose][] defines a multi-container application.
This file describes the services required by powerflow and how to start them.
Moreover, this dockerfile supports some components required for the automation
infrastructure (such as traefik, which is used to broker traffic to staging
and production, but also to trigger either of these deployments).

[docker-compose]: https://docs.docker.com/compose/

### docker-compose.staging.yml

This file describes the components needed by the staging deployment.
Contrary to the production file, this does not contain software necessary to
support the deployment.

### powerflow.service

This [systemd][] service runs the _docker-compose_ for the production
deployment.
It should be installed as a system service and enabled to start at boot.
This can be do so using the following command:

[systemd]: https://systemd.io/

```console
$ sudo ln -sf ~/powerflow/powerflow.service /etc/systemd/system/ && sudo systemctl enable --now powerflow
```

### powerflow-staging@.service

This [systemd][] service runs the _docker-compose_ for the staging
deployment.
The instance parameter refers to the branch (or rather tag) that will be
used to fetch the deployment components.
It should be installed as a system service but not enabled to start at boot.

### webhook.conf

[webhook][] configuration file.
This service executes commands upon receiving actions over HTTP.
This file should be installed under `/etc/webhook.conf`.
See the file for details on what actions are provided.

[webhook]: https://github.com/adnanh/webhook

### traefik/conf/*

This directory contains traeffik configuration files.
Mind that none of these files correspond to the traeffik platform configuration
(which is exclusively done using command line arguments); but rather service
configurations.

For instance, the file used to configure the endpoints for webhooks is here.
On the other hand, the configuration for the staging and production endpoints
is done through the 'docker provider', which inspects the labels of each docker
container and dynamically creates the required configuration.

[]: vim:spell:
