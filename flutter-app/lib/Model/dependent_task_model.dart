import 'package:powerflow/Model/task_model.dart';

class DependentTaskModel {
  DependentTaskModel(this.previous, this.next);

  final DependencyType dependencyType = DependencyType.strong;
  final TaskModel previous;
  final TaskModel next;
}

enum DependencyType {
  weak,
  strong,
}
