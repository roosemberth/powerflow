import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:powerflow/Model/role_model.dart';
import 'package:powerflow/Model/task_model.dart';

import 'account_model.dart';
import 'dependent_task_model.dart';

class ProjectModel extends ChangeNotifier {
  ProjectModel(this._title)
      : _milestones = [],
        _roles = [RoleModel("Developer"), RoleModel("Designer")],
        _members = [
          AccountModel.fromName("Paul"),
          AccountModel.fromName("Jean"),
          AccountModel.fromName("Jacques"),
        ],
        _description = "New project",
        _tasks = [],
        _tasksRelations = [] {
    TaskModel finished = TaskModel(0, state: StateTask.done);
    _milestones.add(TaskModel(1));
    _milestones.add(TaskModel(2));
    _milestones.add(finished);
    _taskId = 3;

    for (var element in _milestones) {
      _tasks.add(element);
    }
  }

  // TODO : Remplacer le clone par une copie des données depuis la DB pour ce mettre a jour ?
  ProjectModel.clone(ProjectModel source)
      : _title = source._title,
        _description = source._description,
        _roles = source._roles,
        _members = source._members,
        _milestones = source._milestones,
        _tasks = source._tasks,
        _tasksRelations = source._tasksRelations,
        _taskId = source._taskId;

  String _title;
  String _description;
  final List<RoleModel> _roles;
  final List<AccountModel> _members;
  final List<TaskModel> _milestones;
  final List<TaskModel> _tasks;
  final List<DependentTaskModel> _tasksRelations;
  int _taskId = 0;

  String get title => _title;

  set title(String value) {
    _title = value;
    notifyListeners();
  }

  String get description => _description;

  set description(String value) {
    _description = value;
    notifyListeners();
  }

  UnmodifiableListView<RoleModel> get roles => UnmodifiableListView(_roles);

  void addRole(RoleModel role) {
    _roles.add(role);
    notifyListeners();
  }

  void modifyRole(RoleModel role, int index) {
    _roles[index] = role;
    notifyListeners();
  }

  void removeRole(int index) {
    _roles.removeAt(index);
    notifyListeners();
  }

  UnmodifiableListView<AccountModel> get members =>
      UnmodifiableListView(_members);

  void addMember(AccountModel role) {
    _members.add(role);
    notifyListeners();
  }

  void removeMember(int index) {
    _members.removeAt(index);
    notifyListeners();
  }

  UnmodifiableListView<TaskModel> get milestones =>
      UnmodifiableListView(_milestones);

  UnmodifiableListView<TaskModel> get tasks => UnmodifiableListView(_tasks);

  UnmodifiableListView<DependentTaskModel> get tasksRelations =>
      UnmodifiableListView(_tasksRelations);

  TaskModel addTask() {
    TaskModel t = TaskModel(_taskId++);
    t.title = "New task " + _taskId.toString();
    _tasks.add(t);
    return t;
  }

  void addRelation(DependentTaskModel d) {
    _tasksRelations.add(d);
  }

  void removeRelation(DependentTaskModel d) {
    _tasksRelations.remove(d);
  }
}
