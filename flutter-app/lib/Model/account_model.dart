import 'dart:collection';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:powerflow/Model/project_model.dart';

class AccountModel extends ChangeNotifier {
  AccountModel()
      : _username = "",
        _projects = [],
        _email = "" {
    var r = Random();
    _username = "User" + r.nextInt(10000).toString();
    _email = r.nextInt(10000).toString() + "@gmail.com";
  }

  AccountModel.fromName(this._username)
      : _projects = [],
        _email = "" {
    var r = Random();
    _email = r.nextInt(10000).toString() + "@gmail.com";
  }

  // TODO : Remplacer le clone par une copie des données depuis la DB pour ce mettre a jour ?
  AccountModel.clone(AccountModel source)
      : _projects = source._projects,
        _username = source._username,
        _email = source._email;

  String _username;
  String _email;
  final List<String> _projects;

  UnmodifiableListView<String> get projects {
    return UnmodifiableListView(_projects);
  }

  void addProject(String name) {
    _projects.add(name);
    notifyListeners();
  }

  String get email {
    return _email;
  }

  String get username {
    return _username;
  }

  set username(String username) {
    _username = username;
    notifyListeners();
  }

  set email(String email) {
    _email = email;
    notifyListeners();
  }

  ProjectModel openProject(String name) {
    return ProjectModel(name);
  }

  ProjectModel createProject() {
    String name = "Project" + Random().nextInt(999).toString();
    addProject(name);
    return ProjectModel(name);
  }
}
