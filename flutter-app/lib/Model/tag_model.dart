import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/task_model.dart';

class TagModel {
  TagModel(this.task, this.tag);

  final TaskModel task;
  final AccountModel? accountAssigned = null;
  final TagType type = TagType.any;
  
  String tag;
}

enum TagType {
  any,
  user,
  role,
  ref,
}
