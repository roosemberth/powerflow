import 'package:powerflow/Model/tag_model.dart';

class TaskModel {
  TaskModel(this._id, {this.state = StateTask.unassigned});

  final int _id;
  final DateTime start = DateTime.now();
  final DateTime end = DateTime.now();
  final StateTask state;
  final List<TagModel> tags = [];

  String title = "Untitled task";
  String description = "";
  Duration estimatedDuration = const Duration();
}

enum StateTask {
  unassigned,
  ready,
  done,
  cancel,
}
