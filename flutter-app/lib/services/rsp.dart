import 'dart:async';
import 'dart:html';

import 'package:dog/dog.dart';
import 'package:flutter/foundation.dart';
import 'package:json_rpc_2/json_rpc_2.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

/// A class for the communication with the Powerflow server services
class RemoteServicesProvider {
  static final _instance = RemoteServicesProvider._();
  RemoteServicesProvider._();
  Client? _backingTransport;
  LoggedInUser? _auth;

  static Future<RemoteServicesProvider> get instance async {
    return _instance;
  }

  Future<Client> get transport async {
    await _connectTransport();
    return _backingTransport!;
  }

  /// Forces the backing transport to be connected
  Future<void> _connectTransport() async {
    if (_backingTransport != null) {
      return;
    }

    String definedUrl = const String.fromEnvironment("SERVER_WS_API");

    final url = Uri.parse(definedUrl.isNotEmpty
        ? definedUrl
        : Uri.base.resolve("/v1/ws").replace(scheme: "wss").toString());

    dog.i("Connecting to server: $url");
    _backingTransport = Client(WebSocketChannel.connect(url).cast<String>());
    dog.i("Connected to server: $url");

    // Never returns
    _backingTransport!.listen().then((_) {
      dog.i("RSP transport was disconnected.");
      _backingTransport = null;
    }).onError((Object e, __) {
      dog.i("RSP transport is disconnected. Error: $e");
      _backingTransport = null;
    });
  }

  Future<bool> logUserIn(String login, String password) async {
    await logUserOut();
    String? bearer = await (await userApi).loginUser(login, password);
    int? userId = await (await userApi).resolveUser(login);
    if (bearer == null || userId == null) {
      return false;
    }
    dog.i("Succesfully logged in as user $login");
    _auth = LoggedInUser(login, userId);
    window.cookieStore?.set("Bearer", bearer);
    (await transport).close(); // Expire the current transport session.
    return true;
  }

  Future<void> logUserOut() async {
    _auth = null;
    // The webapi is missing the 'delete' method. Set an invalid value instead.
    await window.cookieStore?.set("Bearer", "invalid");
  }

  LoggedInUser? get logged => _auth;

  /// Returns a PowerflowServerPingService object used for pinging the server
  static Future<PowerflowServerPingService> get pingApi => instance
      .then((i) => i.transport.then((_) => PowerflowServerPingService(i)));

  /// Returns a proxy to the user service API
  static Future<PowerflowServerUserService> get userApi => instance
      .then((i) => i.transport.then((_) => PowerflowServerUserService(i)));
}

class UserIsNotLoggedIn implements Exception {
  @override
  String toString() => "Exception: User is not logged in.";
}

class LoggedInUser {
  final String loggedUserLogin;
  final int loggedUserId;

  LoggedInUser(this.loggedUserLogin, this.loggedUserId);
}

/// A model class for using the services given by the server
abstract class PowerflowServerService {
  final RemoteServicesProvider rsp;

  PowerflowServerService(this.rsp);

  @protected
  Future<Client> get transport async {
    return await rsp.transport;
  }
}

class PowerflowServerPingService extends PowerflowServerService {
  PowerflowServerPingService(final rsp) : super(rsp);

  Future<String> ping() async {
    dog.i("Sending ping request to server.");
    String result = await (await transport).sendRequest("ping.ping");
    dog.i("Received ping from server.");
    return result;
  }

  Future<String?> whoAmI() async {
    dog.i("Sending whoAmI request.");
    String? result = await (await transport).sendRequest("ping.whoAmI");
    dog.i("Received whoAmI reply from server: " + result.toString());
    return result;
  }
}

class PowerflowServerUserService extends PowerflowServerService {
  PowerflowServerUserService(final rsp) : super(rsp);

  Future<void> registerUser(String login, String name, String password) async {
    return await (await transport)
        .sendRequest("user.registerUser", [login, name, password]);
  }

  Future<String?> loginUser(String login, String password) async {
    return await (await transport)
        .sendRequest("user.loginUser", [login, password]);
  }

  Future<int?> resolveUser(String login) async {
    return await (await transport).sendRequest("user.resolveUser", [login]);
  }

  Future<List<String>> getUserRoles(int userId) async {
    const method = "user.getUserRoles";
    final response = await (await transport).sendRequest(method, [userId]);
    if (response is! List<dynamic>) {
      throw const FormatException("RPC: $method didn't receive List<String>");
    }
    return response.map((r) => r as String).toList();
  }

  Future<void> setUserRoles(int userId, List<String> roles) async {
    return await (await transport)
        .sendRequest("user.setUserRoles", [userId, roles]);
  }

  // getUserWorkingHours & setUserWorkingHours not implemented.
}
