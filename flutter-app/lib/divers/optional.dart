class Optional<T> {
  const Optional.empty() : elem = null;

  const Optional(this.elem);

  final T? elem;
}
