import 'package:flutter/material.dart';

class OutlinedText extends StatelessWidget {
  final String text;
  final double fontSize;
  final Color color;
  final Color strokeColor;
  final double strokeWidth;

  const OutlinedText(
    this.text,
    this.fontSize,
    this.color,
    this.strokeColor,
    this.strokeWidth, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Text(
        text,
        style: TextStyle(
          fontSize: fontSize,
          foreground: Paint()
            ..color = strokeColor
            ..strokeWidth = strokeWidth
            ..style = PaintingStyle.stroke),
      ),
      Text(
        text,
        style: TextStyle(color: color, fontSize: fontSize),
      ),
    ]);
  }
}
