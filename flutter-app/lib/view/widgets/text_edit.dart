import 'package:flutter/material.dart';

class TextEdit extends StatefulWidget {
  final String content;
  final void Function(String text) update;
  final TextStyle? style;
  final int? maxLines;

  const TextEdit(
      {required this.content,
      required this.update,
      this.maxLines,
      this.style,
      Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _TextEditState();
}

class _TextEditState extends State<TextEdit> {
  bool _isInEdit = false;

  @override
  Widget build(BuildContext context) {
    Widget textZone;
    Widget control;
    TextEditingController controller =
        TextEditingController(text: widget.content);

    if (_isInEdit) {
      textZone = TextField(
        maxLines: widget.maxLines,
        controller: controller,
        style: widget.style,
      );

      control = Column(
        children: [
          TextButton(
            child: const Text("save"),
            onPressed: () => setState(
              () {
                widget.update(controller.text);
                _isInEdit = false;
              },
            ),
          ),
          TextButton(
            child: const Text("cancel"),
            onPressed: () => setState(
              () {
                _isInEdit = false;
                controller.text = widget.content;
              },
            ),
          ),
        ],
      );
    } else {
      textZone = Text(
        controller.text,
        style: widget.style,
        maxLines: widget.maxLines,
      );
      control = IconButton(
        icon: const Icon(Icons.edit),
        onPressed: () => setState(() {
          _isInEdit = true;
        }),
      );
    }

    return Row(
      children: <Widget>[
        Expanded(child: textZone),
        control,
      ],
    );
  }
}
