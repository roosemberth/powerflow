import 'package:flutter/material.dart';

Widget boxedWidget(Widget child) {
  return Expanded(
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.0,
          color: const Color.fromARGB(255, 0, 0, 0),
        ),
      ),
      child: child,
    ),
  );
}
