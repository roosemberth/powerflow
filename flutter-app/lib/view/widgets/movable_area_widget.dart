import 'package:flutter/material.dart';

class MovableAreaWidget extends StatefulWidget {
  final List<MovableWidget> initialChildren;

  const MovableAreaWidget(this.initialChildren, {Key? key}) : super(key: key);

  @override
  _MovableAreaWidgetState createState() => _MovableAreaWidgetState();
}

class _MovableAreaWidgetState extends State<MovableAreaWidget> {
  late List<MovableWidget> children;

  @override
  void initState() {
    super.initState();
    children = widget.initialChildren;
  }

  void updatePosition(
      MovableWidget child,
      Offset newPosition,
      Offset? parentPos) {
    if (parentPos == null) {
      return;
    }

    setState(() {
      child.position = newPosition - parentPos;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        for (var i in children)
          Positioned(
            left: i.position.dx,
            top: i.position.dy,
            child: Draggable(
              maxSimultaneousDrags: 1,
              feedback: i.child,
              childWhenDragging: Opacity(
                opacity: .3,
                child: i.child,
              ),
              onDragEnd: (details) =>
                  updatePosition(i, details.offset, context.absolutePosition),
              child: i.child,
            ),
          ),
      ],
    );
  }
}

extension GlobalContextExtension on BuildContext {
  Offset? get absolutePosition {
    final renderObject = findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    if (translation != null) {
      return Offset(translation.x, translation.y);
    } else {
      return null;
    }
  }
}

class MovableWidget {
  Widget child;
  Offset position;

  MovableWidget({required this.position, required this.child});
}
