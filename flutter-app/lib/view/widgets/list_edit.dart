import 'package:flutter/material.dart';
import 'package:powerflow/view/widgets/text_edit.dart';

class ListEdit<T> extends StatelessWidget {
  final String title;
  final void Function() add;
  final String Function(int index) getText;
  final void Function(int index, String text)? update;
  final void Function(int index) remove;

  final void Function(int index)? edit;
  final List<T> data;

  const ListEdit.customEdit(
      {required this.title,
      required this.add,
      required this.getText,
      required this.edit,
      required this.remove,
      required this.data,
      Key? key})
      : update = null,
        super(key: key);

  const ListEdit(
      {required this.title,
      required this.add,
      required this.getText,
      required this.update,
      required this.remove,
      required this.data,
      Key? key})
      : edit = null,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          title,
          style: const TextStyle(fontSize: 24),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (BuildContext context, int index) {
              Widget edit;

              if (this.edit == null) {
                edit = TextEdit(
                  content: getText(index),
                  update: (text) => update!(index, text),
                  maxLines: 1,
                );
              } else {
                edit = Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      getText(index),
                      maxLines: 1,
                    )),
                    IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () => this.edit!(index),
                    ),
                  ],
                );
              }

              return Row(
                children: [
                  Expanded(
                    child: edit,
                  ),
                  IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: () => remove(index),
                  ),
                ],
              );
            },
          ),
        ),
        IconButton(
          icon: const Icon(Icons.add_circle),
          onPressed: () => add(),
        ),
      ],
    );
  }
}
