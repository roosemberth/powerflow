import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DurationEdit extends StatelessWidget {
  final Duration content;
  final void Function(Duration val) update;
  final TextStyle? style;

  late final TextEditingController hoursEditController;
  late final TextEditingController minutesEditController;

  DurationEdit(
      {required this.content, required this.update, this.style, Key? key})
      : super(key: key) {
    hoursEditController =
        TextEditingController(text: content.inHours.toString());
    minutesEditController =
        TextEditingController(text: content.inMinutes.remainder(60).toString());
  }

  static Widget numberText(TextEditingController controller, TextStyle? style,
      String formatter, void Function() update) {
    return TextField(
      keyboardType: TextInputType.number,
      inputFormatters: [FilteringTextInputFormatter.allow(RegExp(formatter))],
      controller: controller,
      onEditingComplete: update,
      style: style,
    );
  }

  Widget numberTextBox(
      TextEditingController controller, TextStyle? style, String formatter) {
    return SizedBox(
      width: 30,
      child: numberText(controller, style, formatter, updateFunc),
    );
  }

  void updateFunc() {
    int hours = hoursEditController.text.isEmpty
        ? 0
        : int.parse(hoursEditController.text);
    int minutes = minutesEditController.text.isEmpty
        ? 0
        : int.parse(minutesEditController.text);

    update(Duration(hours: hours, minutes: minutes));
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        numberTextBox(hoursEditController, style, '^[0123456789]{0,3}\$'),
        SizedBox(
          width: 10,
          child: Text("h", style: style),
        ),
        numberTextBox(minutesEditController, style, '^[012345]?[0123456789]\$'),
      ],
    );
  }
}
