import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:powerflow/Model/dependent_task_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/Model/tag_model.dart';
import 'package:powerflow/Model/task_model.dart';
import 'package:powerflow/view/widgets/Graph/duration_edit.dart';
import 'package:powerflow/view/widgets/Graph/task_block.dart';
import 'package:graphview/GraphView.dart';
import 'package:powerflow/view/widgets/helper.dart';

import '../list_edit.dart';
import '../text_edit.dart';

// Graph from : https://pub.dev/packages/graphview

class GraphArea extends StatefulWidget {
  final ProjectModel project;

  const GraphArea({required this.project, Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GraphAreaState();
}

class _GraphAreaState extends State<GraphArea> {
  final Graph graph = Graph();

  final SugiyamaConfiguration builder = SugiyamaConfiguration()
    ..levelSeparation = (40)
    ..nodeSeparation = (20)
    ..orientation = (BuchheimWalkerConfiguration.ORIENTATION_LEFT_RIGHT);

  TaskModel? focusedTask;
  UnmodifiableListView<DependentTaskModel> listPrevious =
      UnmodifiableListView([]);

  UnmodifiableListView<DependentTaskModel> listNext = UnmodifiableListView([]);

  SelectDependencyType selectDependency2 = SelectDependencyType.none;

  void refreshNextPreced() {
    setState(() {
      if (focusedTask == null) {
        listPrevious = UnmodifiableListView([]);
        listNext = UnmodifiableListView([]);
      } else {
        listPrevious = UnmodifiableListView(widget.project.tasksRelations
            .where((element) => element.next == focusedTask));

        listNext = UnmodifiableListView(widget.project.tasksRelations
            .where((element) => element.previous == focusedTask));
      }
    });
  }

  void updateFocusedTask(TaskModel? task) {
    if (selectDependency2 == SelectDependencyType.none) {
      setState(() {
        focusedTask = task;
      });
    } else {
      if (task == null || task == focusedTask) {
        selectDependency2 = SelectDependencyType.none;
        return;
      }
      TaskModel previous;
      TaskModel next;

      if (selectDependency2 == SelectDependencyType.previous) {
        previous = task;
        next = focusedTask!;
      } else {
        next = task;
        previous = focusedTask!;
      }

      DependentTaskModel newDependency = DependentTaskModel(previous, next);

      widget.project.addRelation(newDependency);
      selectDependency2 = SelectDependencyType.none;
      graph.addEdge(
          Node.Id(newDependency.previous), Node.Id(newDependency.next));
    }
    refreshNextPreced();
  }

  void removeDependency(DependentTaskModel dependence) {
    widget.project.removeRelation(dependence);
    graph.removeEdgeFromPredecessor(
        Node.Id(dependence.previous), Node.Id(dependence.next));

    refreshNextPreced();
  }

  @override
  void initState() {
    super.initState();

    for (TaskModel t in widget.project.tasks) {
      graph.addNode(Node.Id(t));
    }

    // Create edge
    for (DependentTaskModel d in widget.project.tasksRelations) {
      if (d.dependencyType == DependencyType.weak) {
        graph.addEdge(Node.Id(d.previous), Node.Id(d.next),
            paint: Paint()..color = Colors.red);
      } else {
        graph.addEdge(Node.Id(d.previous), Node.Id(d.next));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: GestureDetector(
            onTap: () {
              updateFocusedTask(null);
            },
            child: InteractiveViewer(
              boundaryMargin: const EdgeInsets.all(double.infinity),
              constrained: false,
              child: GraphView(
                graph: graph,
                algorithm: SugiyamaAlgorithm(builder),
                builder: (Node node) {
                  return GestureDetector(
                    onTap: () {
                      updateFocusedTask(node.key!.value as TaskModel);
                    },
                    child: TaskBlock(
                      task: node.key!.value as TaskModel,
                      highlight: focusedTask == node.key!.value,
                    ),
                  );
                },
              ),
            ),
          ),
        ),
        if (focusedTask != null)
          Expanded(
            child: Column(
              children: [
                const Divider(color: Colors.black),
                Row(
                  children: [
                    Expanded(
                      child: TextEdit(
                        content: focusedTask!.title,
                        update: (text) => setState(() {
                          focusedTask!.title = text;
                        }),
                        style: const TextStyle(fontSize: 24),
                        maxLines: 1,
                      ),
                    ),
                    Column(
                      children: [
                        const Text("Estimation",
                            style: TextStyle(fontSize: 12)),
                        DurationEdit(
                          content: focusedTask!.estimatedDuration,
                          update: (val) => setState(
                            () {
                              focusedTask!.estimatedDuration = val;
                            },
                          ),
                          style: const TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ],
                ),
                const Divider(),
                const Align(
                  alignment: Alignment.topLeft,
                  child: Text("Description", style: TextStyle(fontSize: 18)),
                ),
                TextEdit(
                  content: focusedTask!.description,
                  update: (text) => setState(() {
                    focusedTask!.description = text;
                  }),
                  style: const TextStyle(fontSize: 12),
                ),
                Expanded(
                  child: Row(
                    children: [
                      boxedWidget(
                        Column(
                          children: [
                            const Text("Previous",
                                style: TextStyle(fontSize: 24)),
                            Expanded(
                              child: ListView.builder(
                                itemCount: listPrevious.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Row(
                                    children: [
                                      Text(listPrevious[index].previous.title),
                                      IconButton(
                                        icon: const Icon(Icons.delete),
                                        onPressed: () => removeDependency(
                                            listPrevious[index]),
                                      )
                                    ],
                                  );
                                },
                              ),
                            ),
                            if (selectDependency2 == SelectDependencyType.none)
                              IconButton(
                                icon: const Icon(Icons.add_circle),
                                onPressed: () => setState(
                                  () {
                                    selectDependency2 =
                                        SelectDependencyType.previous;
                                  },
                                ),
                              ),
                          ],
                        ),
                      ),
                      boxedWidget(
                        ListEdit<TagModel>(
                          title: "Members",
                          data: focusedTask!.tags,
                          add: () => focusedTask!.tags
                              .add(TagModel(focusedTask!, "new Tag")),
                          getText: (int index) =>
                              focusedTask!.tags.elementAt(index).tag,
                          update: (int index, String s) =>
                              focusedTask!.tags.elementAt(index).tag = s,
                          remove: (int index) =>
                              focusedTask!.tags.removeAt(index),
                        ),
                      ),
                      boxedWidget(
                        Column(
                          children: [
                            const Text("Next", style: TextStyle(fontSize: 24)),
                            Expanded(
                              child: ListView.builder(
                                itemCount: listNext.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Row(
                                    children: [
                                      Text(listNext[index].next.title),
                                      IconButton(
                                        icon: const Icon(Icons.delete),
                                        onPressed: () =>
                                            removeDependency(listNext[index]),
                                      )
                                    ],
                                  );
                                },
                              ),
                            ),
                            if (selectDependency2 == SelectDependencyType.none)
                              IconButton(
                                icon: const Icon(Icons.add_circle),
                                onPressed: () => setState(
                                  () {
                                    selectDependency2 =
                                        SelectDependencyType.next;
                                  },
                                ),
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                ElevatedButton(
                  child: const Text('addTask'),
                  onPressed: () {
                    setState(
                      () {
                        TaskModel t = widget.project.addTask();
                        graph.addNode(Node.Id(t));
                      },
                    );
                  },
                ),
              ],
            ),
          ),
      ],
    );
  }
}

enum SelectDependencyType { none, next, previous }
