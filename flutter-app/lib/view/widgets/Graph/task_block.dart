import 'package:flutter/material.dart';
import 'package:powerflow/Model/task_model.dart';

class TaskBlock extends StatelessWidget {
  final TaskModel task;
  final bool highlight;

  static const Color colorDefault = Color.fromARGB(255, 0, 0, 0);
  static const Color colorHighlight = Color.fromARGB(255, 0, 0, 255);

  const TaskBlock({required this.task, this.highlight = false, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 100,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.0,
          color: highlight ? colorHighlight : colorDefault,
        ),
      ),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1.0,
                  color: highlight ? colorHighlight : colorDefault,
                ),
              ),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    task.title,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontSize: 18),
                  ),
                ),
                SizedBox(
                  width: 70,
                  child: Column(
                    children: [
                      const Text(
                        "Estimation",
                        style: TextStyle(fontSize: 9),
                      ),
                      Text(
                        task.estimatedDuration.inHours.toString() +
                            "h" +
                            task.estimatedDuration.inMinutes
                                .remainder(60)
                                .toString(),
                        style: const TextStyle(fontSize: 9),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                task.description,
                style: const TextStyle(fontSize: 12),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
