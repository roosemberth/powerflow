import 'package:flutter/material.dart';

import '../movable_area_widget.dart';

class LandingPageBackground extends StatelessWidget {
  const LandingPageBackground({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MovableAreaWidget(
      // Movable objects
      [
        MovableWidget(
          position: const Offset(600, 100),
          child: Image.asset(
            'assets/images/DashboardShowcase.png',
            width: 600,
            height: 400,
          ),
        ),
        MovableWidget(
          position: const Offset(400, 500),
          child: Image.asset(
            'assets/images/PlanningShowcase.png',
            width: 600,
            height: 400,
          ),
        ),
        MovableWidget(
          position: const Offset(1000, 300),
          child: Image.asset(
            'assets/images/TaskShowcase.png',
            width: 600,
            height: 400,
          ),
        ),
      ],
    );
  }
}
