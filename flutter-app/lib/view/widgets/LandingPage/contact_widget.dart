import 'package:flutter/material.dart';

class ContactWidget extends StatelessWidget {
  const ContactWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Align(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Table(
            columnWidths: const {
              0: IntrinsicColumnWidth(),
              1: IntrinsicColumnWidth(),
              2: IntrinsicColumnWidth(),
            },
            children: [
              TableRow(children: [
                Container(),
                const Text(
                  "Development team",
                  style: TextStyle(color: Colors.white),
                ),
                Container(),
              ]),
              TableRow(children: [
                const Text(
                  "Junod Christophe",
                  style: TextStyle(color: Colors.white),
                ),
                Container(),
                const Text(
                  "Ngueukam Djeuda Wilfried Karel",
                  style: TextStyle(color: Colors.white),
                ),
              ]),
              TableRow(children: [
                const Text(
                  "Gazzetta Florian",
                  style: TextStyle(color: Colors.white),
                ),
                Container(),
                const Text(
                  "Palacios Roosembert",
                  style: TextStyle(color: Colors.white),
                )
              ])
            ],
          ),
        ),
      ),
    );
  }
}
