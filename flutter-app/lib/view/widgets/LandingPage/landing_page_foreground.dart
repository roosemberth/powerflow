import 'package:flutter/material.dart';

import '../outlined_text.dart';

class LandingPageForeground extends StatelessWidget {
  const LandingPageForeground({Key? key}) : super(key: key);

  OutlinedText _mkLabel(text) =>
      OutlinedText(text, 36, Colors.white, Colors.black, 2);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50),
      child: Align(
        alignment: Alignment.centerLeft,
        child: ClipRect(
          child: OverflowBox(
            maxHeight: 500,
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _mkLabel("- Automate your\n  project planning"),
                _mkLabel(""),
                _mkLabel("- Organise your team\n  tasks"),
                _mkLabel(""),
                _mkLabel("- Keep your project\n  deadlines")
              ],
            ),
          ),
        ),
      ),
    );
  }
}
