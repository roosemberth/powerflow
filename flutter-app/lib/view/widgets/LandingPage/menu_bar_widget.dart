import 'package:flutter/material.dart';
import 'package:powerflow/view/pages/menu_connexion.dart';
import '../outlined_text.dart';

class MenuBarWidget extends StatelessWidget {
  const MenuBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.lightBlue,
      child: Padding(
        padding: const EdgeInsets.all(25),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          alignment: WrapAlignment.spaceAround,
          spacing: 10,
          children: [
            const OutlinedText(
                "POWER WORKFLOW", 72, Colors.white, Colors.black, 3),
            ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.grey),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const MenuConnexion()),
                );
              },
              child: const OutlinedText(
                  "LOGIN", 40, Colors.white, Colors.black, 2),
            ),
          ],
        ),
      ),
    );
  }
}
