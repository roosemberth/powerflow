import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/view/widgets/Project/project.dart';

class ExistingProjectWidget extends ProjectWidget {
  final String projectName;

  const ExistingProjectWidget(this.projectName, {Key? key}) : super(key: key);

  @override
  Widget createChild() {
    return Text(
      projectName,
      style: const TextStyle(
        fontSize: 32.0,
      ),
    );
  }

  @override
  ProjectModel getProject(AccountModel model) {
    return model.openProject(projectName);
  }
}
