import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/view/layouts/project_overview_layout.dart';
import 'package:powerflow/view/pages/app_generic_layout.dart';
import 'package:provider/provider.dart';

abstract class ProjectWidget extends StatelessWidget {
  const ProjectWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      height: 150,
      child: ElevatedButton(
        onPressed: () {
          AccountModel model =
              Provider.of<AccountModel>(context, listen: false);

          ProjectModel project = getProject(model);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (newContext) {
                return AppGenericLayout(ProjectOverviewLayout(model, project));
              },
            ),
          );
        },
        child: createChild(),
      ),
    );
  }

  ProjectModel getProject(AccountModel model);

  Widget createChild();
}
