import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/view/widgets/Project/project.dart';

class NewProjectWidget extends ProjectWidget {
  const NewProjectWidget({Key? key}) : super(key: key);

  @override
  Widget createChild() {
    return const Icon(Icons.add);
  }

  @override
  ProjectModel getProject(AccountModel model) {
    return model.createProject();
  }
}
