import 'package:flutter/material.dart';
import 'package:powerflow/view/pages/screen_size_widget.dart';

import '../widgets/LandingPage/contact_widget.dart';
import '../widgets/LandingPage/menu_bar_widget.dart';
import '../widgets/LandingPage/landing_page_background.dart';
import '../widgets/LandingPage/landing_page_foreground.dart';

class LandingPage extends ScreenSizeWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context, ScreenSize size) {
    List<Widget> elems = [Container(color: Colors.lightBlue)];
    if (size.index > ScreenSize.large.index) {
      elems.add(const Opacity(opacity: 0.5, child: LandingPageBackground()));
    }
    elems.add(const LandingPageForeground());

    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const MenuBarWidget(),
          Expanded(
            child: Stack(
              children: elems,
            ),
          ),
          const ContactWidget(),
        ],
      ),
    );
  }
}
