import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/view/layouts/project_list_layout.dart';
import 'package:powerflow/view/pages/screen_size_widget.dart';

import 'app_generic_layout.dart';

class MenuConnexion extends ScreenSizeWidget {
  const MenuConnexion({Key? key}) : super(key: key);

  @override
  Widget buildContent(BuildContext context, ScreenSize size) {
    return Material(
      child: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 150),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  child: const Text('Login'),
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AppGenericLayout(
                              ProjectListLayout(AccountModel()))),
                    );
                  },
                ),
              ),
              const SizedBox(height: 40),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  child: const Text('Register'),
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
