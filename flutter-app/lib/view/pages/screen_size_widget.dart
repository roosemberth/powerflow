import 'package:flutter/material.dart';

enum ScreenSize {
  mini,
  small,
  medium,
  large,
  XL,
  XXL,
}

abstract class ScreenSizeWidget extends StatelessWidget {
  const ScreenSizeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buildContent(context, _getScreenSize(context));
  }

  ScreenSize _getScreenSize(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    if (width > 1536) {
      return ScreenSize.XXL;
    } else if (width > 1280) {
      return ScreenSize.XL;
    } else if (width > 1024) {
      return ScreenSize.large;
    } else if (width > 768) {
      return ScreenSize.medium;
    } else if (width > 640) {
      return ScreenSize.small;
    } else {
      return ScreenSize.mini;
    }
  }

  Widget buildContent(BuildContext context, ScreenSize size);
}
