import 'package:flutter/material.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/divers/optional.dart';
import 'package:powerflow/view/layouts/app_layout.dart';
import 'package:powerflow/view/layouts/calendar_layout.dart';
import 'package:powerflow/view/layouts/graph_layout.dart';
import 'package:powerflow/view/layouts/profile_layout.dart';
import 'package:powerflow/view/layouts/project_list_layout.dart';
import 'package:powerflow/view/layouts/project_overview_layout.dart';
import 'package:powerflow/view/pages/screen_size_widget.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class AppGenericLayout extends ScreenSizeWidget {
  const AppGenericLayout(this.child, {Key? key}) : super(key: key);

  final AppLayout child;

  PopupMenuItem<Optional<MaterialPageRoute<Never>>> menuItem(
      String text, AppLayout layout) {
    return PopupMenuItem<Optional<MaterialPageRoute<Never>>>(
      child: Text(text),
      value: Optional(
        MaterialPageRoute<Never>(
            builder: (context) => AppGenericLayout(layout)),
      ),
    );
  }

  @override
  Widget buildContent(BuildContext context, ScreenSize size) {
    ProjectModel? project = child.getProject();

    var providers = <SingleChildWidget>[
      ChangeNotifierProvider(create: (_) {
        return child.account;
      })
    ];

    if (project != null) {
      providers.add(ChangeNotifierProvider(create: (_) {
        return project;
      }));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(project == null ? "POWER WORKFLOW" : project.title),
        leading: PopupMenuButton(
          icon: const Icon(Icons.menu),
          color: Colors.blue,
          itemBuilder: (context) {
            List<PopupMenuEntry<Optional<MaterialPageRoute<Never>>>> list = [];

            list.add(menuItem("Projects", ProjectListLayout(child.account)));
            list.add(menuItem("Manage account", ProfileLayout(child.account)));

            list.add(const PopupMenuItem<Optional<MaterialPageRoute<Never>>>(
                child: Text("Sign out"),
                value: Optional<MaterialPageRoute<Never>>.empty()));

            if (project != null) {
              list.add(const PopupMenuDivider());
              list.add(menuItem(
                  "Dashboard", ProjectOverviewLayout(child.account, project)));
              list.add(
                  menuItem("Planning", CalendarLayout(child.account, project)));
              list.add(
                  menuItem("Tasks graph", GraphLayout(child.account, project)));
            }

            return list;
          },
          onSelected: (Optional<MaterialPageRoute<Never>> item) {
            MaterialPageRoute? route = item.elem;
            if (route == null) {
              Navigator.pop(context);
            } else {
              Navigator.pushReplacement(context, route);
            }
          },
        ),
      ),
      body: MultiProvider(
          providers: providers,
          builder: (context, oldChild) {
            return child;
          }),
    );
  }
}
