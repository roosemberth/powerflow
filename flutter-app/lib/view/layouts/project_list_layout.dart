import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/view/widgets/Project/existing_project.dart';
import 'package:powerflow/view/widgets/Project/new_project.dart';
import 'package:powerflow/view/widgets/Project/project.dart';
import 'package:provider/provider.dart';

import 'global_layout.dart';

class ProjectListLayout extends GlobalLayout {
  ProjectListLayout(AccountModel account, {Key? key})
      : super(account, key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(50.0),
      child: Consumer<AccountModel>(
        builder: (context, model, child) {
          List<ProjectWidget> children = [];

          for (var element in model.projects) {
            children.add(ExistingProjectWidget(element));
          }

          children.add(const NewProjectWidget());

          return Wrap(runSpacing: 24.0, spacing: 24.0, children: children);
        },
      ),
    );
  }
}
