import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'app_layout.dart';

abstract class GlobalLayout extends AppLayout {
  GlobalLayout(AccountModel account, {Key? key}) : super(account, key: key);

  @override
  ProjectModel? getProject() {
    return null;
  }
}
