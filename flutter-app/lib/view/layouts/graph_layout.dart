import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/view/layouts/project_layout.dart';
import 'package:powerflow/view/widgets/Graph/graph_area.dart';

class GraphLayout extends ProjectLayout {
  GraphLayout(AccountModel account, ProjectModel project, {Key? key})
      : super(account, project, key: key);

  @override
  Widget build(BuildContext context) {
    return GraphArea(project: project);
  }
}
