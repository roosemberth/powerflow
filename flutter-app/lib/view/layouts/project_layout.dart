import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';

import 'app_layout.dart';

abstract class ProjectLayout extends AppLayout {
  ProjectLayout(AccountModel account, source, {Key? key})
      : project = ProjectModel.clone(source), super(account, key: key);

  final ProjectModel project;

  @override
  ProjectModel getProject() {
    return project;
  }
}
