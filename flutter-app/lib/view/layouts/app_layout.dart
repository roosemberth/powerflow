import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';

abstract class AppLayout extends StatelessWidget {
  AppLayout(AccountModel source, {Key? key})
      : account = AccountModel.clone(source),
        super(key: key);

  final AccountModel account;

  ProjectModel? getProject();
}
