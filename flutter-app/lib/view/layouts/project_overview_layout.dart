import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/Model/role_model.dart';
import 'package:powerflow/Model/task_model.dart';
import 'package:powerflow/view/layouts/project_layout.dart';
import 'package:powerflow/view/widgets/helper.dart';
import 'package:powerflow/view/widgets/list_edit.dart';
import 'package:powerflow/view/widgets/text_edit.dart';
import 'package:provider/provider.dart';

class ProjectOverviewLayout extends ProjectLayout {
  ProjectOverviewLayout(AccountModel account, ProjectModel project, {Key? key})
      : super(account, project, key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Column(
            children: [
              Container(
                height: 50,
                decoration: BoxDecoration(
                  border: Border.all(
                      width: 1.0, color: const Color.fromARGB(255, 0, 0, 0)),
                ),
              ),
              Container(
                height: 200,
                decoration: BoxDecoration(
                  border: Border.all(
                      width: 1.0, color: const Color.fromARGB(255, 0, 0, 0)),
                ),
                child: Column(
                  children: [
                    const Text(
                      "Description",
                      style: TextStyle(fontSize: 24),
                    ),
                    Consumer<ProjectModel>(
                      builder: (context, model, child) {
                        return TextEdit(
                          content: model.description,
                          update: (text) => model.description = text,
                        );
                      },
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    boxedWidget(
                      Consumer<ProjectModel>(
                        builder: (context, model, child) {
                          return ListEdit<RoleModel>(
                            title: "Role",
                            data: model.roles,
                            add: () => model.addRole(RoleModel("unnamed")),
                            getText: (int index) =>
                                model.roles.elementAt(index).name,
                            update: (int index, String s) =>
                                model.modifyRole(RoleModel(s), index),
                            remove: (int index) => model.removeRole(index),
                          );
                        },
                      ),
                    ),
                    boxedWidget(
                      Consumer<ProjectModel>(
                        builder: (context, model, child) {
                          return ListEdit<AccountModel>.customEdit(
                            title: "Members",
                            data: model.members,
                            add: () => model
                                .addMember(AccountModel.fromName("unnamed")),
                            getText: (int index) =>
                                model.members.elementAt(index).username,
                            edit: (int index) {
                              //TODO  : implementer l'ajout et la suppresion de role
                            },
                            remove: (int index) => model.removeMember(index),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            decoration: const BoxDecoration(
              border: Border(
                left:
                    BorderSide(width: 1.0, color: Color.fromARGB(255, 0, 0, 0)),
              ),
            ),
            child: Column(
              children: [
                const Text(
                  "Status du projet",
                  style: TextStyle(fontSize: 24),
                ),
                Expanded(
                  child: Consumer<ProjectModel>(
                    builder: (context, model, child) {
                      return ListView.builder(
                        itemCount: model.milestones.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Row(
                            children: [
                              if (model.milestones.elementAt(index).state ==
                                  StateTask.done)
                                const Icon(Icons.check)
                              else
                                const Icon(Icons.arrow_forward),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(model.milestones.elementAt(index).title),
                                  Text(model.milestones
                                      .elementAt(index)
                                      .end
                                      .toString()),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
