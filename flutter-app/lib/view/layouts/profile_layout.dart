import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/view/widgets/text_edit.dart';
import 'package:provider/provider.dart';

import 'global_layout.dart';

class ProfileLayout extends GlobalLayout {
  ProfileLayout(AccountModel account, {Key? key}) : super(account, key: key);

  SizedBox btn(String text, void Function() action) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        child: Text(text),
        onPressed: action,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(children: [
              Row(
                children: [
                  const Text("Username : "),
                  Expanded(
                    child: Consumer<AccountModel>(
                      builder: (context, model, child) {
                        return TextEdit(
                          content: model.username,
                          update: (text) => model.username = text,
                          maxLines: 1,
                        );
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 40),
              Row(
                children: [
                  const Text("Email : "),
                  Expanded(
                    child: Consumer<AccountModel>(
                      builder: (context, model, child) {
                        return TextEdit(
                          content: model.email,
                          update: (text) => model.email = text,
                          maxLines: 1,
                        );
                      },
                    ),
                  ),
                ],
              ),
            ]),
            const SizedBox(height: 40),
            btn('Modify password', () {}),
            const SizedBox(height: 40),
            btn('Delete account', () {
              Navigator.pop(context);
            })
          ],
        ),
      ),
    );
  }
}
