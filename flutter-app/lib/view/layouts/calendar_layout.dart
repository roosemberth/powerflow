import 'package:flutter/material.dart';
import 'package:powerflow/Model/account_model.dart';
import 'package:powerflow/Model/project_model.dart';
import 'package:powerflow/view/layouts/project_layout.dart';

class CalendarLayout extends ProjectLayout {
  CalendarLayout(AccountModel account, ProjectModel project, {Key? key})
      : super(account, project, key: key);

  @override
  Widget build(BuildContext context) {
    return const Center();
  }
}
