# Powerflow web client

This directory contains the Powerflow green client (henceforth the client).

This project is built using [flutter][].
You can find details documentation about flutter on their website.
A reference workflow is provided in this document for convenience.
The reference workflow assumes you have the flutter SDK installed.
Please refer to [the installation guide][instal-flutter] if needed.

[flutter]: https://flutter.dev/
[install-flutter]: https://flutter.dev/docs/get-started/install

## Reference workflow

You can make sure the necessary flutter components are correctly installed in
your system by running `flutter doctor`, in particular this project requires
the 'Web' (chrome) component.
You can override the chrome binary using the `CHROME_EXECUTABLE` environment
variable.

You may run the web application by running
`flutter run --dart-define=SERVER_WS_API=ws://localhost:8091/v1/ws` (assuming
the server is reachable at localhost:8091).
This will open a chrome instance with the application.

## Release build

To build the release artifact of the app use the following command:

```console
flutter build web --release
```
