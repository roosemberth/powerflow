# Powerflow

Powerflow is a workflow manager with assisted group task planning.

This project is work-in-progress. The rest of the README details the expected
design decisions.

## Platform architecture

```mermaid
graph LR
  subgraph View["Client"]
    Web["Web (flutter)"]
  end
  subgraph Server
    subgraph Application server
      Srv["fa:fa-cogs Application server (Spring boot fa:fa-leaf)"]
    end
    subgraph Infra
      Pgsql["fa:fa-database Database (PostgreSQL)"]
    end
  end
  Web -->|HTTPS| Srv
  Srv --> Pgsql
```
