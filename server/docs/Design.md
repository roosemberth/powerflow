---
title: Design of the powerflow server
author: Roosembert Palacios
---

## Communication with the server

The server communicates via [JSON-RPC 2.0][jrpc2].
This interface may be reached at `/v1/rpc` over HTTP.
We expect to make this interface available over websockets as well.

[jrpc2]: https://www.jsonrpc.org/specification#response_object

The following reasons motivated our use of jsonrpc:

- Easy batching of requests and responses thus minimizing round trips.
- Easy to 'mount' over different transports (e.g. HTTP, Web sockets).
- Flexible, yet well-defined semantics, such as the use of JSON objects and
  extensible error codes.
- Easy extensibility to make real-time applications (for instance, the server
  can notify clients about information they didn't request, such as updated
  entities).
- The RPC semantics allow us to define precise method signatures to perform
  actions instead of exposing collections with special behaviour.
- Little cost over other schemas (such as REST).

## Entity Relationship Diagram

Please look at the latest version of the entity sets stored in the database
[in the ERDs directory](./ERDs).

[]: vim:spell:spelllang=en
