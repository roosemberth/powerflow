package ch.heig.pdg2021.server.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.heig.pdg2021.server.db.UserCredentialRepository;
import ch.heig.pdg2021.server.db.UserRepository;
import ch.heig.pdg2021.server.model.entity.User;
import ch.heig.pdg2021.server.model.entity.UserCredential;
import ch.heig.pdg2021.server.service.BearerTokenService;
import ch.heig.pdg2021.server.service.ConnectionInfoService;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ConfigurationTest {
    @TestConfiguration
    public static class TestConfig {
        @RestController
        public static class TestController {
            @Autowired
            ConnectionInfoService connectionInfoService;

            @RequestMapping("/ep1")
            public String testEndpoint1() {
                return connectionInfoService.getUser().get().getName();
            }

            @RequestMapping("/ep/2")
            public String testEndpoint2() {
                return connectionInfoService.getUser().get().getName();
            }
        }
    }

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCredentialRepository userCredentialsRepository;

    @Autowired
    private BearerTokenService bearerTokenService;

    @Autowired
    private MockMvc mvc;

    @Test
    void userIsSetFromBearer() throws Exception {
        String name = "Some user";
        String login = "username";
        User u = userRepository.save(User.builder().name(name).login(login).build());
        userCredentialsRepository.save(UserCredential.builder().user(u).password("Some password").build());
        String token = bearerTokenService.newToken(u);

        mvc.perform(get("/ep1").header("Bearer", token)).andExpect(content().string(name));
        mvc.perform(get("/ep/2").header("Bearer", token)).andExpect(content().string(name));
    }
}
