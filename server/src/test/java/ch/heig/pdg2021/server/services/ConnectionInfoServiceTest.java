package ch.heig.pdg2021.server.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.heig.pdg2021.server.config.ThreadScopeConfig;
import ch.heig.pdg2021.server.db.UserRepository;
import ch.heig.pdg2021.server.model.entity.User;
import ch.heig.pdg2021.server.service.ConnectionInfoService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { ThreadScopeConfig.class, ConnectionInfoService.class })
public class ConnectionInfoServiceTest {
    @Autowired
    ConnectionInfoService underTest;

    @MockBean
    UserRepository userRepository;

    @Test
    public void setUserThenClear() {
        User u = User.builder().name("ExistingUser").build();

        when(userRepository.findUserByLogin(u.getName())).thenReturn(Optional.of(u));
        assertTrue(underTest.getUser().isEmpty());
        underTest.setUser(u.getName());
        assertEquals(Optional.of(u), underTest.getUser());
        underTest.clearUser();
        assertTrue(underTest.getUser().isEmpty());

        when(userRepository.findUserByLogin(anyString())).thenReturn(Optional.empty());
        underTest.setUser("Unexistant");
        assertTrue(underTest.getUser().isEmpty());
    }
}
