package ch.heig.pdg2021.server.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.heig.pdg2021.server.db.UserCredentialRepository;
import ch.heig.pdg2021.server.db.UserRepository;
import ch.heig.pdg2021.server.model.entity.User;
import ch.heig.pdg2021.server.model.entity.UserCredential;
import ch.heig.pdg2021.server.service.BearerTokenService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { BearerTokenService.class, ObjectMapper.class })
public class BearerTokenServiceTest {
    @Autowired
    BearerTokenService underTest;

    @MockBean
    UserCredentialRepository userCredentialsRepository;

    @MockBean
    UserRepository userRepository;

    @Test
    public void canGetUserFromNewToken() {
        String login = "SomeUsername";
        User u = User.builder().name("Some user").login(login).build();
        UserCredential c = UserCredential.builder().user(u).password("Some password").build();

        when(userRepository.findUserByLogin(login)).thenReturn(Optional.of(u));
        when(userCredentialsRepository.findByUserLogin(login)).thenReturn(Optional.of(c));

        assertEquals(Optional.of(u), underTest.fromToken(underTest.newToken(u)));
    }
}
