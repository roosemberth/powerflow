package ch.heig.pdg2021.server.integration;

import static org.junit.jupiter.api.Assertions.*;

import java.net.URL;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import ch.heig.pdg2021.server.service.IPing;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PingTest {
    @LocalServerPort
    int port;

    IPing api;

    @BeforeEach
    public void setupApi() throws Exception {
        URL url = new URL("http://localhost:" + port + "/v1/rpc");
        JsonRpcHttpClient client = new JsonRpcHttpClient(url);
        api = RpcUtilsForTests.createProxyFromClient("ping", getClass().getClassLoader(), IPing.class, client);
    }

    @Test
    public void pingReturnsPong() {
        assertEquals("pong", api.ping());
    }
}
