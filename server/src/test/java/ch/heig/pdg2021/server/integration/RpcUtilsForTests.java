package ch.heig.pdg2021.server.integration;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.JsonRpcMethod;
import com.googlecode.jsonrpc4j.ReflectionUtil;

public class RpcUtilsForTests {
    public static String getRpcMethodName(Method method) {
        JsonRpcMethod jsonRpcMethod = ReflectionUtil.getAnnotation(method, JsonRpcMethod.class);
        if (jsonRpcMethod == null) {
            return method.getName();
        } else {
            return jsonRpcMethod.value();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T createProxyFromClient(String name, ClassLoader classLoader, Class<T> proxyInterface,
            JsonRpcHttpClient client) {
        return (T) Proxy.newProxyInstance(classLoader, new Class<?>[] { proxyInterface }, (proxy, method, args) -> {
            final Object arguments = ReflectionUtil.parseArguments(method, args);
            final String methodName = name + "." + getRpcMethodName(method);
            return client.invoke(methodName, arguments, method.getGenericReturnType());
        });

    }
}
