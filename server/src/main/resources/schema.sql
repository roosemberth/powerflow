create sequence hibernate_sequence start 1 increment 1;

    create table dependencies (
       principal_id int8 not null,
        dependency_id int8 not null,
        type varchar(255) not null
    );

    create table milestones (
       id int8 not null,
        due DATE,
        project_id int8 not null,
        primary key (id)
    );

    create table milestones_tasks (
       milestones_id int8 not null,
        tasks_id int8 not null
    );

    create table projects (
       id int8 not null,
        name varchar(255) not null,
        creator_id int8 not null,
        primary key (id)
    );

    create table roles (
       name varchar(255) not null,
        primary key (name)
    );

    create table sub_tasks (
       principal_id int8 not null,
        subtask_id int8 not null
    );

    create table task_assignations (
       id int8 not null,
        assignee_id int8,
        task_id int8,
        primary key (id)
    );

    create table task_plannings (
       id int8 not null,
        ts_end TIMESTAMP,
        ts_start TIMESTAMP,
        principal_id int8,
        task_id int8,
        primary key (id)
    );

    create table tasks (
       id int8 not null,
        description varchar(255) not null,
        estimated_duration INTERVAL DAY TO SECOND,
        start_date_max TIME,
        start_date_min TIME,
        state varchar(255) not null,
        title varchar(255) not null,
        creator_id int8 not null,
        project_id int8 not null,
        primary key (id)
    );

    create table task_work_logs (
       id int8 not null,
        ts_end TIMESTAMP not null,
        ts_start TIMESTAMP not null,
        principal_id int8 not null,
        task_id int8 not null,
        primary key (id)
    );

    create table user_credentials (
       id int8 not null,
        password varchar(255) not null,
        user_id int8 not null,
        primary key (id)
    );

    create table user_roles (
       principal int8 not null,
        role varchar(255) not null
    );

    create table user_roles_in_project (
       principal_id int8 not null,
        role_name varchar(255) not null,
        project_id int8 not null,
        primary key (role_name, project_id, principal_id)
    );

    create table users (
       id int8 not null,
        login varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    );

    create table working_hours (
       id int8 not null,
        day varchar(255) not null,
        ts_end TIME not null,
        ts_start TIME not null,
        user_id int8 not null,
        primary key (id)
    );

    alter table if exists sub_tasks 
       add constraint UK_grmf9r1e9tiyo1s9d093lhj13 unique (subtask_id);

    alter table if exists task_assignations 
       add constraint UK5he55lv4wq79ey4f073645ma2 unique (assignee_id, task_id);

    alter table if exists user_credentials 
       add constraint UK_thx1lw5kg5ygi8d8b90gv2ha3 unique (user_id);

    alter table if exists dependencies 
       add constraint FKm5d6so25w96secfjf77k235da 
       foreign key (dependency_id) 
       references tasks;

    alter table if exists dependencies 
       add constraint FK5xsp6wr1x9m5vqn7dp2c2o1d2 
       foreign key (principal_id) 
       references tasks;

    alter table if exists milestones 
       add constraint FK2a7fp7wfu0qc1pq3wiifbksge 
       foreign key (project_id) 
       references projects;

    alter table if exists milestones_tasks 
       add constraint FKglmh88c5gd2mvm7opx8tnaavr 
       foreign key (tasks_id) 
       references tasks;

    alter table if exists milestones_tasks 
       add constraint FKd87vkt2yrm8l318da9qt44u14 
       foreign key (milestones_id) 
       references milestones;

    alter table if exists projects 
       add constraint FK14mww7skdu5cpg6nq2kwcnx0e 
       foreign key (creator_id) 
       references users;

    alter table if exists sub_tasks 
       add constraint FKhm8q1ga9nfqu4mnw1l2f6e5hc 
       foreign key (subtask_id) 
       references tasks;

    alter table if exists sub_tasks 
       add constraint FKf2t7xeuqcbrbp7mtj078e0jp7 
       foreign key (principal_id) 
       references tasks;

    alter table if exists task_assignations 
       add constraint FKq8vldc4qiq8svppeu73w7ui45 
       foreign key (assignee_id) 
       references users;

    alter table if exists task_assignations 
       add constraint FKoo1dxsf696qslh1bih3g1p82v 
       foreign key (task_id) 
       references tasks;

    alter table if exists task_plannings 
       add constraint FKajdy8i1vw5d3f37givdo7nl7q 
       foreign key (principal_id) 
       references users;

    alter table if exists task_plannings 
       add constraint FK6pgbr3j2grtr5u9p4ywpkumjc 
       foreign key (task_id) 
       references tasks;

    alter table if exists tasks 
       add constraint FKt1ph5sat39g9lpa4g5kl46tbv 
       foreign key (creator_id) 
       references users;

    alter table if exists tasks 
       add constraint FKsfhn82y57i3k9uxww1s007acc 
       foreign key (project_id) 
       references projects;

    alter table if exists task_work_logs 
       add constraint FKk3n80nkwpv86lfuwcrtb242fj 
       foreign key (principal_id) 
       references users;

    alter table if exists task_work_logs 
       add constraint FKifj8h33jkxjaxv7aaesrh35ov 
       foreign key (task_id) 
       references tasks;

    alter table if exists user_credentials 
       add constraint FK98kxj78ausx1xo94eq4mkjm9q 
       foreign key (user_id) 
       references users;

    alter table if exists user_roles 
       add constraint FKaq6gmj85gm0d11gvpe4rj8l2n 
       foreign key (role) 
       references roles;

    alter table if exists user_roles 
       add constraint FKljyd1ryg4aijqdbcw0248xhhk 
       foreign key (principal) 
       references users;

    alter table if exists user_roles_in_project 
       add constraint FK39oy7072y0canacuytfwb61dn 
       foreign key (principal_id) 
       references users;

    alter table if exists user_roles_in_project 
       add constraint FK65750mfdl2q8ccfs6xxihlbex 
       foreign key (role_name) 
       references roles;

    alter table if exists user_roles_in_project 
       add constraint FKra92gevne4vnc9wd1hc3rtsnk 
       foreign key (project_id) 
       references projects;

    alter table if exists working_hours 
       add constraint FKpy10xmimpg66989h8quu4b5lw 
       foreign key (user_id) 
       references users;
