package ch.heig.pdg2021.server.model.entity;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "Tasks")
public class Task {
    enum State {
        Draft, Created, Done, Cancelled;
    }

    @Getter
    @Setter
    @Embeddable
    public static class Dependency {
        enum Type {
            Strong, Weak;
        }

        // Principal is not mentioned here since it will be added when the
        // class is embedded.

        @ManyToOne(optional = false)
        private Task dependency;

        @Column(nullable = false)
        @Enumerated(EnumType.STRING)
        private Type type;
    }

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    private Project project;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @OneToMany
    @JoinTable(name = "SubTasks", joinColumns = @JoinColumn(name = "principal_id"), inverseJoinColumns = @JoinColumn(name = "subtask_id"))
    private Collection<Task> subTasks;

    @ElementCollection
    @CollectionTable(name = "Dependencies", joinColumns = @JoinColumn(name = "principal_id"))
    private Collection<Dependency> dependencies;

    @ManyToOne(optional = false)
    private User creator;

    @Column(columnDefinition = "INTERVAL DAY TO SECOND")
    private Duration estimatedDuration;

    @Column(columnDefinition = "TIME")
    private LocalDateTime startDateMin;

    @Column(columnDefinition = "TIME")
    private LocalDateTime startDateMax;
}
