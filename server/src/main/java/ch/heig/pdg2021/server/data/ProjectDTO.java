package ch.heig.pdg2021.server.data;

import ch.heig.pdg2021.server.model.entity.User;
import lombok.Data;

@Data
public class ProjectDTO {
    private Long id;

    private String name;

    private User creator;
}
