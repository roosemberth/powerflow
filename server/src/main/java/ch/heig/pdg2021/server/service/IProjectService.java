package ch.heig.pdg2021.server.service;

import ch.heig.pdg2021.server.data.ProjectDTO;
import ch.heig.pdg2021.server.model.entity.Task;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

public interface IProjectService {
    /**
     * Creates a new Project.
     *
     * The owner is infered from the currently connected user.
     */
    ProjectDTO createProject(String name);

    /**
     * Adds a member with the specified roles to a project.
     *
     * This operation is idempotent. If the specified user is already a member, it sets their roles to the specified
     * list. If the role list is empty, the user is removed from the project.
     */
    void setUserProjectMembership(Long projectId, Long userId, Collection<String> roles);

    /**
     * Returns the collection of tasks belonging to the specified project.
     */
    Collection<Task> getProjectTasks(Long projectId);

    /**
     * Returns the tasks for a given user within a project.
     */
    Collection<Task> getTasksForUserInProject(Long projectId, Long userId);

    /**
     * Adds a milestone to the project.
     *
     * @return The ID of the newly created milestone.
     */
    Long addProjectMilestone(Long projectId, Optional<LocalDate> due);

    /**
     * Returns the tasks associated with the specified milestone.
     */
    Collection<Task> getMilestoneTasks(Long milestoneId);

    /**
     * Sets the tasks which are part of the specified milestone.
     */
    void setMilestoneTasks(Long milestoneId, Collection<Long> taskIds);
}
