package ch.heig.pdg2021.server.model.entity;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "UserRolesInProject")
public class UserRolesInProject implements Serializable {
    @Id
    @ManyToOne
    private Project project;

    @Id
    @ManyToOne
    private User principal;

    @Id
    @ManyToOne
    private Role role;
}
