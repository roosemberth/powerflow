package ch.heig.pdg2021.server.model.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "UserCredentials")
public class UserCredential {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(optional = false)
    private User user;

    @Column(nullable = false)
    private String password;
}
