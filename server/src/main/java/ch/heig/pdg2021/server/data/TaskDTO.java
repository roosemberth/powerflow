package ch.heig.pdg2021.server.data;

import lombok.Data;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
public class TaskDTO {

    enum State {
        Draft, Created, Done, Cancelled;
    }

    private Long id;

    private State state;

    private String title;

    private String description;

    private Duration estimateDuration;

    private LocalDateTime startDateMin;

    private LocalDateTime startDateMax;
}
