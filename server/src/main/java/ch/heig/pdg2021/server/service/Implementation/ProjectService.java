package ch.heig.pdg2021.server.service.Implementation;

import ch.heig.pdg2021.server.db.*;
import ch.heig.pdg2021.server.data.ProjectDTO;
import ch.heig.pdg2021.server.model.entity.*;
import ch.heig.pdg2021.server.service.ConnectionInfoService;
import ch.heig.pdg2021.server.service.IProjectService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private MilestoneRepository milestoneRepository;

    @Autowired
    private TaskAssignationRepository taskAssignationRepository;

    @Autowired
    private ConnectionInfoService connectionInfoService;

    public ProjectDTO createProject(ProjectDTO projectDto) {
        Project project = new Project();
        project.setCreator(projectDto.getCreator());
        project.setName(projectDto.getName());
        Project storeProject = projectRepository.save(project);

        ProjectDTO returnValue = new ProjectDTO();
        BeanUtils.copyProperties(storeProject, returnValue);

        return returnValue;
    }

    /**
     * Creates a new Project.
     * <p>
     * The owner is infered from the currently connected user.
     *
     * @param name
     */
    @Override
    public ProjectDTO createProject(String name) {
        User creator = connectionInfoService.getUser()
                .orElseThrow(() -> new RuntimeException("A logged-in user is required."));
        ProjectDTO returnValue = new ProjectDTO();
        Project project = new Project();
        project.setCreator(creator);
        project.setName(name);
        projectRepository.save(project);

        BeanUtils.copyProperties(project, returnValue);

        return returnValue;
    }

    /**
     * Adds a member with the specified roles to a project.
     * <p>
     * This operation is idempotent. If the specified user is already a member, it sets their roles to the specified
     * list. If the role list is empty, the user is removed from the project.
     *
     * @param projectId
     * @param userId
     * @param roles
     */
    @Override
    public void setUserProjectMembership(Long projectId, Long userId, Collection<String> roles) {
        Project project = projectRepository.findProjectById(projectId)
                .orElseThrow(() -> new RuntimeException(String.format("project with id %d doesn't exist", projectId)));

        User user = userRepository.findUserById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("user with id %d doesn't exist", userId)));

        Collection<UserRolesInProject> newRoles = roles.stream().map(roleName -> {
            Role role = roleRepository.findRoleByName(roleName)
                    .orElseGet(() -> roleRepository.save(new Role(roleName)));
            UserRolesInProject ret = new UserRolesInProject();
            ret.setProject(project);
            ret.setPrincipal(user);
            ret.setRole(role);
            return ret;
        }).collect(Collectors.toList());

        Collection<UserRolesInProject> updated = project.getMembers().stream()
                .filter(m -> m.getPrincipal().getId() != userId).collect(Collectors.toList());
        updated.addAll(newRoles);

        project.setMembers(updated);
        projectRepository.save(project);
    }

    /**
     * Returns the collection of tasks belonging to the specified project.
     *
     * @param projectId
     */
    @Override
    public Collection<Task> getProjectTasks(Long projectId) {
        projectRepository.findProjectById(projectId)
                .orElseThrow(() -> new RuntimeException(String.format("project with id %d doesn't exist", projectId)));

        return taskRepository.findTaskByProject(projectId);
    }

    /**
     * Returns the tasks for a given user within a project.
     *
     * @param projectId
     * @param userId
     */
    @Override
    public Collection<Task> getTasksForUserInProject(Long projectId, Long userId) {
        Optional<Project> existingProject = projectRepository.findProjectById(projectId);
        if (existingProject.isEmpty()) {
            throw new RuntimeException(String.format("project with id %d doesn't exist", projectId));
        }

        Optional<User> existingUser = userRepository.findUserById(userId);
        if (existingUser.isEmpty()) {
            throw new RuntimeException(String.format("user with id %d doesn't exist", userId));
        }

        Project project = existingProject.get();
        User user = existingUser.get();

        Collection<Task> tasks = taskAssignationRepository.findByAssignee(userId);

        if (tasks.isEmpty()) {
            return new ArrayList<>();
        }

        return tasks.stream().filter(t -> t.getProject().getId() == projectId).collect(Collectors.toSet());
    }

    /**
     * Adds a milestone to the project.
     *
     * @param projectId
     * @param due
     * 
     * @return The ID of the newly created milestone.
     */
    @Override
    public Long addProjectMilestone(Long projectId, Optional<LocalDate> due) {
        Optional<Project> existingProject = projectRepository.findProjectById(projectId);
        if (existingProject.isEmpty()) {
            throw new RuntimeException(String.format("project with id %d doesn't exist", projectId));
        }

        Project project = existingProject.get();
        LocalDate dueMilestone = due.orElse(null);

        Milestone milestone = new Milestone();
        milestone.setProject(project);
        if (dueMilestone != null) {
            milestone.setDue(dueMilestone);
        }

        Milestone returnValue = milestoneRepository.save(milestone);

        return returnValue.getId();
    }

    /**
     * Returns the tasks associated with the specified milestone.
     *
     * @param milestoneId
     */
    @Override
    public Collection<Task> getMilestoneTasks(Long milestoneId) {
        Optional<Milestone> existingMilestone = milestoneRepository.findMilestoneById(milestoneId);

        if (existingMilestone.isEmpty()) {
            throw new RuntimeException(String.format("Milestone with id %d doesn't exist", milestoneId));
        }

        return existingMilestone.get().getTasks();
    }

    /**
     * Sets the tasks which are part of the specified milestone.
     *
     * @param milestoneId
     * @param taskIds
     */
    @Override
    public void setMilestoneTasks(Long milestoneId, Collection<Long> taskIds) {
        Optional<Milestone> existingMilestone = milestoneRepository.findMilestoneById(milestoneId);

        if (existingMilestone.isEmpty()) {
            throw new RuntimeException(String.format("Milestone with id %d doesn't exist", milestoneId));
        }

        Milestone milestone = existingMilestone.get();

        milestone.setTasks(taskIds.stream().map(t -> {
            Task task = taskRepository.findTaskById(t).orElseThrow(() -> {
                throw new RuntimeException(String.format("Task with Id %d doesn't exist", t));
            });
            return task;
        }).collect(Collectors.toCollection(HashSet::new)));

        milestoneRepository.save(milestone);
    }
}
