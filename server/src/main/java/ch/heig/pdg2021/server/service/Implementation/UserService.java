package ch.heig.pdg2021.server.service.Implementation;

import ch.heig.pdg2021.server.data.WorkingHourDTO;
import ch.heig.pdg2021.server.db.WorkingHourRepository;
import ch.heig.pdg2021.server.model.entity.Role;
import ch.heig.pdg2021.server.model.entity.WorkingHour;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.heig.pdg2021.server.db.RoleRepository;
import ch.heig.pdg2021.server.db.UserCredentialRepository;
import ch.heig.pdg2021.server.db.UserRepository;
import ch.heig.pdg2021.server.model.entity.User;
import ch.heig.pdg2021.server.model.entity.UserCredential;
import ch.heig.pdg2021.server.service.BearerTokenService;
import ch.heig.pdg2021.server.service.IUserService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserCredentialRepository credentialRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    BearerTokenService bearerTokenService;

    @Autowired
    WorkingHourRepository workingHourRepository;

    /**
     * Register a new User. The 'User' role is automatically assigned.
     *
     * @param login
     * @param name
     * @param password
     */
    @Override
    public void registerUser(String login, String name, String password) {
        Optional<User> existingUser = userRepository.findUserByLogin(login);

        if (existingUser.isPresent()) {
            throw new RuntimeException(String.format("user with login %s already exist", login));
        }

        Role userRole = roleRepository.findRoleByName("User").orElseGet(() -> {
            return roleRepository.save(new Role("User"));
        });

        // Saving the user in the database
        User user = new User();
        user.setLogin(login);
        user.setName(name);
        user.setRoles(List.of(userRole));
        User persistedUser = userRepository.save(user);

        // Saving the user credentials in the database
        UserCredential credential = new UserCredential();
        credential.setPassword(password);
        credential.setUser(persistedUser);
        credentialRepository.save(credential);
    }

    /**
     * Log an existing user in.
     *
     * @param user
     * @param password
     * 
     * @return A bearer token requests MUST use to authenticate the user.
     */
    @Override
    public String loginUser(String login, String password) {
        var unacceptableException = new RuntimeException(
                "The specified credentials are not acceptable to log the user in.");
        UserCredential cred = credentialRepository.findByUserLogin(login).orElseThrow(() -> unacceptableException);
        if (!cred.getPassword().equals(password))
            throw unacceptableException;
        return bearerTokenService.newToken(cred.getUser());
    }

    /**
     * Retrieve the roles associated to an user.
     *
     * @param userId
     */
    @Override
    public Long resolveUser(String login) {
        return userRepository.findUserByLogin(login).map(u -> u.getId()).orElse(null);
    }

    /**
     * Retrieve the roles associated to an user.
     *
     * @param userId
     */
    @Override
    public Collection<String> getUserRoles(Long userId) {
        Optional<User> existingUser = userRepository.findUserById(userId);
        if (existingUser.isEmpty()) {
            throw new RuntimeException(String.format("user with Id %s doesn't exist", userId));
        }
        User user = existingUser.get();
        return user.getRoles().stream().map(r -> r.getName()).collect(Collectors.toCollection(HashSet::new));
    }

    /**
     * Set the roles an user can use.
     *
     * @param userId
     * @param roles
     */
    @Override
    public void setUserRoles(Long userId, Collection<String> roles) {
        Optional<User> existingUser = userRepository.findUserById(userId);

        if (existingUser.isEmpty()) {
            throw new RuntimeException(String.format("user with Id %s doesn't exist", userId));
        }

        User user = existingUser.get();
        Collection<Role> userRoles = roles.stream().map(r -> new Role(r))
                .collect(Collectors.toCollection(HashSet::new));

        user.setRoles(userRoles);

        userRepository.save(user);

    }

    /**
     * Retrieves the working hours of a user.
     *
     * @param userId
     */
    @Override
    public Collection<WorkingHourDTO> getUserWorkingHours(Long userId) {
        if (userRepository.findUserById(userId).isEmpty()) {
            throw new RuntimeException(String.format("user with Id %s doesn't exist", userId));
        }

        return workingHourRepository.findByUser(userId).stream().map(w -> {
            WorkingHourDTO wDto = new WorkingHourDTO();
            BeanUtils.copyProperties(w, wDto);
            return wDto;
        }).collect(Collectors.toList());
    }

    /**
     * Sets the working hours for a user.
     *
     * @param userId
     * @param workingHours
     */
    @Override
    public void setUserWorkingHours(Long userId, Collection<WorkingHourDTO> workingHours) {
        User user = userRepository.findUserById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("user with Id %s doesn't exist", userId)));

        Collection<WorkingHour> updated = workingHours.stream().map(wDto -> {
            WorkingHour w = new WorkingHour();
            BeanUtils.copyProperties(wDto, w);
            w.setUser(user);
            return w;
        }).collect(Collectors.toList());

        workingHourRepository.deleteByUser(userId);
        workingHourRepository.saveAll(updated);
    }
}
