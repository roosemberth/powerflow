package ch.heig.pdg2021.server.model.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "TaskAssignations")
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "assignee_id", "task_id" }) })
public class TaskAssignation {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Task task;

    @ManyToOne
    private User assignee;
}
