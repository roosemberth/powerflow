package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

    Optional<Task> findTaskById(Long Id);

    Optional<Task> findTaskBySubTasksContaining(Task subTask);

    Collection<Task> findTaskByProject(Long projectId);
}
