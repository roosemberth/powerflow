package ch.heig.pdg2021.server.service;

import java.util.Optional;

import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;

public interface IPing {
    @JsonRpcErrors({ @JsonRpcError(exception = RuntimeException.class, code = -32603) })
    String ping();

    @JsonRpcErrors({ @JsonRpcError(exception = RuntimeException.class, code = -32603) })
    String whoAmI();
}
