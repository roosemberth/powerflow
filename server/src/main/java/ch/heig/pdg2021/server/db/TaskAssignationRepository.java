package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.Task;
import ch.heig.pdg2021.server.model.entity.TaskAssignation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface TaskAssignationRepository extends CrudRepository<TaskAssignation, Long> {
    Collection<Task> findByAssignee(Long userId);
}
