package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    Optional<Project> findProjectByName(String name);

    Optional<Project> findProjectById(Long id);
}
