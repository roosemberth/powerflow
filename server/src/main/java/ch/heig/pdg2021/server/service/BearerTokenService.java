package ch.heig.pdg2021.server.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.Optional;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.heig.pdg2021.server.db.UserRepository;
import ch.heig.pdg2021.server.db.UserCredentialRepository;
import ch.heig.pdg2021.server.model.entity.User;
import ch.heig.pdg2021.server.model.entity.UserCredential;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Service
public class BearerTokenService {
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class Container {
        String login;
        long epoch;
        String hmac;
    }

    @Autowired
    ObjectMapper om;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserCredentialRepository userCredentialsRepository;

    /**
     * Inconditionally generates a new token.
     */
    public String newToken(User user) {
        try {
            long now = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
            String login = user.getLogin();
            String hmac = sign(login, Long.toString(now), login);
            String token = om.writeValueAsString(new Container(login, now, hmac));
            return Base64.getEncoder().encodeToString(token.getBytes("utf-8"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns a User if the Token is valid.
     */
    public Optional<User> fromToken(String token) {
        try {
            String decoded = new String(Base64.getDecoder().decode(token), "utf-8");
            Container c = om.readValue(decoded, Container.class);
            String login = c.getLogin();
            String expectedHmac = sign(login, Long.toString(c.getEpoch()), login);
            if (!expectedHmac.equals(c.getHmac()))
                return Optional.empty();
            return userRepository.findUserByLogin(login);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private String sign(String login, String salt, String data) throws NoSuchAlgorithmException, InvalidKeyException {
        UserCredential c = userCredentialsRepository.findByUserLogin(login)
                .orElseThrow(() -> new RuntimeException("The specified user does not exist."));
        String key = salt + "-" + c.getPassword();

        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(key.getBytes(), "HmacSHA256"));
        return Base64.getEncoder().encodeToString(mac.doFinal(data.getBytes()));
    }
}
