package ch.heig.pdg2021.server;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.jsonrpc4j.InvocationListener;
import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;
import com.googlecode.jsonrpc4j.JsonRpcMultiServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.HttpRequestHandler;

import ch.heig.pdg2021.server.service.IPing;
import ch.heig.pdg2021.server.service.IUserService;

@SpringBootApplication
public class ServerApplication {
    @Autowired
    private ApplicationContext ctx;

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Bean(name = "/v1/rpc")
    public HttpRequestHandler rpcRequestHandler(@Autowired JsonRpcMultiServer jsonRpcHandler) {
        return jsonRpcHandler::handle;
    }

    @Bean
    public JsonRpcMultiServer getJsonRpcMultiServer() {
        JsonRpcMultiServer exporter = new JsonRpcMultiServer();

        // Finer control of logged exceptions
        exporter.setShouldLogInvocationErrors(false);
        exporter.setInvocationListener(getUnregisteredExceptionsInvocationListener());

        // Mount services to the jsonrpc API
        exporter.addService("ping", ctx.getBean(IPing.class), IPing.class);
        exporter.addService("user", ctx.getBean(IUserService.class), IUserService.class);

        return exporter;
    }

    /**
     * {@link InvocationListener} logging unregistered {@link Exception}s.
     *
     * Returns an RPC {@link InvocationListener} which logs any {@link Exception} thrown by any RPC API service if such
     * is not 'registered' using a {@link JsonRpcError} clause.
     */
    private InvocationListener getUnregisteredExceptionsInvocationListener() {
        return new InvocationListener() {
            @Override
            public void willInvoke(Method method, List<JsonNode> arguments) {
                // Nothing to do here.
            }

            @Override
            public void didInvoke(Method method, List<JsonNode> arguments, Object result, Throwable t, long duration) {
                if (t == null)
                    return;

                Throwable exceptionThrownByApi = t.getCause();

                if (!isRegisteredException(exceptionThrownByApi, method))
                    exceptionThrownByApi.printStackTrace();
            }

            private boolean isRegisteredException(Throwable t, Method method) {
                JsonRpcErrors registeredErrors = method.getAnnotation(JsonRpcErrors.class);
                if (registeredErrors == null)
                    return false;

                for (JsonRpcError registeredError : registeredErrors.value())
                    if (registeredError.exception().isAssignableFrom(t.getClass()))
                        return true;

                return false;
            }
        };
    }
}
