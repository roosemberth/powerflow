package ch.heig.pdg2021.server.service.Implementation;

import ch.heig.pdg2021.server.data.TaskDTO;
import ch.heig.pdg2021.server.db.TaskRepository;
import ch.heig.pdg2021.server.model.entity.Task;
import ch.heig.pdg2021.server.service.ITasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TaskService implements ITasksService {

    @Autowired
    private TaskRepository taskRepository;

    /**
     * Creates a new task.
     *
     * @param projectId
     * @param title
     * @param description
     */
    @Override
    public void createTask(Long projectId, String title, String description) {

    }

    /**
     * Updates all properties of a task.
     *
     * @param value
     */
    @Override
    public void setTaskProperties(TaskDTO value) {
        Task task = taskRepository.findTaskById(value.getId())
                .orElseThrow(() -> new RuntimeException("A task with Id " + value.getId() + " doesn't exists"));
        task.setDescription(value.getDescription());
        task.setTitle(value.getTitle());
        task.setEstimatedDuration(value.getEstimateDuration());
        task.setStartDateMin(value.getStartDateMin());
        task.setStartDateMax(value.getStartDateMax());

        taskRepository.save(task);
    }

    /**
     * Marks a task as a subordinate task.
     * <p>
     * If the subjectTaskId is null, any subtask relationship of the specified subtask is cleared. If a subtask
     * relationship already exists it is replaced. This operation is idempotent.
     *
     * @param subjectTaskId
     * @param subTaskId
     */
    @Override
    public void setSubTask(Long subjectTaskId, Long subTaskId) {
        Task subtask = taskRepository.findTaskById(subTaskId)
                .orElseThrow(() -> new RuntimeException("A task with Id " + subTaskId + " doesn't exists"));
        if (subjectTaskId == null) { // Clear any subtask relationship
            Optional<Task> parentTask = taskRepository.findTaskBySubTasksContaining(subtask);

            parentTask.ifPresent(task -> task
                    .setSubTasks(task.getSubTasks().stream().filter(t -> !Objects.equals(t.getId(), subTaskId))
                            .collect(Collectors.toCollection(HashSet::new))));
        } else {
            Task subjectTask = taskRepository.findTaskById(subjectTaskId)
                    .orElseThrow(() -> new RuntimeException("A task with Id " + subjectTaskId + " doesn't exists"));

            // Prevents cycle subtask relationship
            if (subtask.getSubTasks().contains(subjectTask)) {
                throw new RuntimeException(
                        "Task with ID " + subjectTaskId + " is a subtask of task with ID " + subTaskId);
            }

            Collection<Task> newSubtasks = subjectTask.getSubTasks();
            newSubtasks.add(subtask);
            subjectTask.setSubTasks(newSubtasks);
            taskRepository.save(subjectTask);
        }

    }

    /**
     * Marks a task as the dependency of another task.
     * <p>
     * If {dependsExists} is not specified, it is assumed true. If {dependsExists} is set to false, the dependency
     * relationship is removed.
     *
     * @param subjectTaskId
     * @param dependencyTaskId
     * @param dependsExists
     */
    @Override
    public void setDependencyRelation(Long subjectTaskId, Long dependencyTaskId, Boolean dependsExists) {

    }

    /**
     * Assigns the specified task to a user.
     * <p>
     * If {isAssignedTo} is not specified it is set to true. If {isAssignedTo} is set to false, the assignation relation
     * is cleared.
     *
     * @param taskId
     * @param userId
     * @param isAssignedTo
     */
    @Override
    public void setUserTaskAssignation(Long taskId, Long userId, Boolean isAssignedTo) {

    }
}
