package ch.heig.pdg2021.server.service.Implementation;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.heig.pdg2021.server.service.ConnectionInfoService;
import ch.heig.pdg2021.server.service.IPing;

@Service
public class Ping implements IPing {
    private Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private ConnectionInfoService connectionInfoService;

    public String ping() {
        logger.info("Processing ping request!");
        return "pong";
    }

    public String whoAmI() {
        return connectionInfoService.getUser().map(u -> u.getName()).orElse(null);
    }
}
