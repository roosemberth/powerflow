package ch.heig.pdg2021.server.model.entity;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "TaskPlannings")
public class TaskPlanning {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Task task;

    @ManyToOne
    private User principal;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime ts_start;

    // TODO(@Roos): Add constraint ts_end > ts_start.
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime ts_end;
}
