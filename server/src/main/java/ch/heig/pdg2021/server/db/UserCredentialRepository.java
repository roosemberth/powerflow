package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.User;
import ch.heig.pdg2021.server.model.entity.UserCredential;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCredentialRepository extends CrudRepository<UserCredential, Long> {
    Optional<UserCredential> findByUserLogin(String login);
}
