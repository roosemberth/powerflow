package ch.heig.pdg2021.server.model.entity;

import java.time.LocalDate;
import java.util.Collection;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "Milestones")
public class Milestone {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    private Project project;

    /**
     * Set of tasks needed to acomplish this milestone.
     *
     * Mind that tasks needed to accomplish this milestone may be part of a different project.
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private Collection<Task> tasks;

    @Column(columnDefinition = "DATE")
    private LocalDate due;
}
