package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findUserByLogin(String login);

    Optional<User> findUserById(Long userId);
}
