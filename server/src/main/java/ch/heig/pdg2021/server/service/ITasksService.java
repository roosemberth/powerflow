package ch.heig.pdg2021.server.service;

import ch.heig.pdg2021.server.data.TaskDTO;

public interface ITasksService {
    /**
     * Creates a new task.
     */
    void createTask(Long projectId, String title, String description);

    /**
     * Updates all properties of a task.
     */
    void setTaskProperties(TaskDTO value);

    /**
     * Marks a task as a subordinate task.
     *
     * If the subjectTask is null, any subtask relationship of the specified subtask is cleared. If a subtask
     * relationship already exists it is replaced. This operation is idempotent.
     */
    void setSubTask(Long subjectTask, Long subTaskId);

    /**
     * Marks a task as the dependency of another task.
     *
     * If {dependsExists} is not specified, it is assumed true. If {dependsExists} is set to false, the dependency
     * relationship is removed.
     */
    void setDependencyRelation(Long subjectTaskId, Long dependencyTaskId, Boolean dependsExists);

    /**
     * Assigns the specified task to a user.
     *
     * If {isAssignedTo} is not specified it is set to true. If {isAssignedTo} is set to false, the assignation relation
     * is cleared.
     */
    void setUserTaskAssignation(Long taskId, Long userId, Boolean isAssignedTo);
}
