package ch.heig.pdg2021.server.config;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.heig.pdg2021.server.service.BearerTokenService;
import ch.heig.pdg2021.server.service.ConnectionInfoService;

@Configuration
public class BearerFilterConfig {
    @Autowired
    private BearerTokenService bearerTokenService;

    @Autowired
    private ConnectionInfoService connectionInfoService;

    @Bean
    public FilterRegistrationBean<HttpFilter> bearerFilter() {
        var reg = new FilterRegistrationBean<HttpFilter>();
        reg.setFilter(new HttpFilter() {
            private void setUserFromBearer(HttpServletRequest req) {
                Optional.ofNullable(req.getHeader("Bearer")).flatMap(bearerTokenService::fromToken)
                        .map(u -> u.getLogin()).ifPresent(connectionInfoService::setUser);
            }

            @Override
            protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
                    throws IOException, ServletException {
                setUserFromBearer(req);
                chain.doFilter(req, res);
            }
        });
        reg.addUrlPatterns("/*");
        return reg;
    }
}
