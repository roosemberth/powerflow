package ch.heig.pdg2021.server.data;

import lombok.Data;

@Data
public class UserDTO {
    private Long id;

    private String name;

    private String login;
}
