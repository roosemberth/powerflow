package ch.heig.pdg2021.server.model.entity;

import java.time.LocalTime;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "WorkingHours")
public class WorkingHour {
    enum Day {
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday;
    }

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    private User user;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Day day;

    @Column(columnDefinition = "TIME", nullable = false)
    private LocalTime ts_start;

    // TODO(@Roos): Add constraint ts_end > ts_start.
    @Column(columnDefinition = "TIME", nullable = false)
    private LocalTime ts_end;
}
