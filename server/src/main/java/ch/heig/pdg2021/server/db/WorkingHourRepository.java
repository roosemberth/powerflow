package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.WorkingHour;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface WorkingHourRepository extends CrudRepository<WorkingHour, Long> {
    Collection<WorkingHour> findByUser(Long userId);

    /**
     * Deletes all the WorkingHour entities associated to the specified user.
     */
    void deleteByUser(Long userId);
}
