package ch.heig.pdg2021.server.model.entity;

import java.util.Collection;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Users")
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String login;

    @ManyToMany
    @JoinTable(name = "UserRoles", joinColumns = @JoinColumn(name = "principal"), inverseJoinColumns = @JoinColumn(name = "role"))
    private Collection<Role> roles;
}
