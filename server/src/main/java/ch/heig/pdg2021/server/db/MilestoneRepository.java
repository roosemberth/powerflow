package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.Milestone;
import ch.heig.pdg2021.server.model.entity.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface MilestoneRepository extends CrudRepository<Milestone, Long> {

    Optional<Milestone> findMilestoneById(Long id);
}
