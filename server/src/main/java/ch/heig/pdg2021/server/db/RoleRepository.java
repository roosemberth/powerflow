package ch.heig.pdg2021.server.db;

import ch.heig.pdg2021.server.model.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, String> {
    Optional<Role> findRoleByName(String name);
}
