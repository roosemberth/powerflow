package ch.heig.pdg2021.server.service;

import ch.heig.pdg2021.server.data.UserCredentialDTO;
import ch.heig.pdg2021.server.data.UserDTO;
import ch.heig.pdg2021.server.data.WorkingHourDTO;
import ch.heig.pdg2021.server.model.entity.WorkingHour;

import java.util.Collection;

public interface IUserService {
    /**
     * Register a new User. The 'User' role is automatically assigned.
     */
    void registerUser(String user, String name, String password);

    /**
     * Log an existing user in.
     *
     * @return A bearer token requests MUST use to authenticate the user.
     */
    String loginUser(String user, String password);

    /**
     * Retrieve the roles associated to an user.
     */
    Collection<String> getUserRoles(Long userId);

    /**
     * Retrieve the userId corresponding to the specified username.
     */
    Long resolveUser(String username);

    /**
     * Set the roles an user can use.
     */
    void setUserRoles(Long userId, Collection<String> roles);

    /**
     * Retrieves the working hours of a user.
     */
    Collection<WorkingHourDTO> getUserWorkingHours(Long userId);

    /**
     * Sets the working hours for a user.
     */
    void setUserWorkingHours(Long userId, Collection<WorkingHourDTO> workingHours);
}
