package ch.heig.pdg2021.server.data;

import lombok.Data;

@Data
public class UserCredentialDTO {
    private Long id;

    private String password;
}
