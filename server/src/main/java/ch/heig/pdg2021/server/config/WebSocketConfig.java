package ch.heig.pdg2021.server.config;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.Cookie;

import com.googlecode.jsonrpc4j.JsonRpcMultiServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import ch.heig.pdg2021.server.service.BearerTokenService;
import ch.heig.pdg2021.server.service.ConnectionInfoService;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Autowired
    private BearerTokenService bearerTokenService;

    @Autowired
    private ConnectionInfoService connectionInfoService;

    @Autowired
    private JsonRpcMultiServer jsonRpcHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        var wsHandler = new TextWebSocketHandler() {
            public void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
                // Check whether a user was already authenticated in this session.
                Optional.ofNullable(session.getAttributes().get("Bearer"))
                        .flatMap(t -> String.class.isInstance(t) ? Optional.of((String) t) : Optional.empty())
                        .ifPresent(connectionInfoService::setUser);
                // Try to set authentication from request headers.
                Optional.ofNullable(session.getHandshakeHeaders().get("Bearer")).flatMap(l -> l.stream().findAny())
                        .flatMap(bearerTokenService::fromToken).map(u -> u.getLogin())
                        .ifPresent(connectionInfoService::setUser);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                InputStream in = new ByteArrayInputStream(message.asBytes());
                jsonRpcHandler.handleRequest(in, out);
                session.sendMessage(new TextMessage(out.toByteArray()));
            }
        };

        var interceptor = new HandshakeInterceptor() {
            private Cookie[] getCookies(ServerHttpRequest request) {
                if (request instanceof ServletServerHttpRequest) {
                    ServletServerHttpRequest serverRequest = (ServletServerHttpRequest) request;
                    return serverRequest.getServletRequest().getCookies();
                }
                return null;
            }

            @Override
            public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                    WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
                Optional.ofNullable(getCookies(request)).map(Arrays::stream).ifPresent(ss -> {
                    ss.filter(c -> "bearer".equals(c.getName().toLowerCase()))
                            .flatMap(c -> bearerTokenService.fromToken(c.getValue()).stream()).findAny()
                            .ifPresent(u -> attributes.put("Bearer", u.getLogin()));
                });
                return true;
            }

            @Override
            public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response,
                    WebSocketHandler wsHandler, Exception exception) {
                // Nothing to do here.
            }
        };

        registry.addHandler(wsHandler, "/v1/ws").setAllowedOrigins("*").addInterceptors(interceptor);
    }
}
