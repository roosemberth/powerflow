package ch.heig.pdg2021.server.service;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import ch.heig.pdg2021.server.db.UserRepository;
import ch.heig.pdg2021.server.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Service
public class ConnectionInfoService {
    @Component
    @Scope("thread")
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ConnectionInfo {
        @Getter
        @Setter
        private Optional<User> user = Optional.empty();
    }

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private UserRepository userRepository;

    private ConnectionInfo getInfo() {
        return Objects.requireNonNull(ctx.getBean(ConnectionInfo.class));
    }

    public void setUser(String login) {
        getInfo().setUser(userRepository.findUserByLogin(login));
    }

    public void clearUser() {
        getInfo().setUser(Optional.empty());
    }

    public Optional<User> getUser() {
        return getInfo().getUser();
    }
}
