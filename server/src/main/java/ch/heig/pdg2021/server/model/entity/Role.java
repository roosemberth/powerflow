package ch.heig.pdg2021.server.model.entity;

import javax.persistence.*;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Roles")
public class Role {
    @Id
    String name;
}
