package ch.heig.pdg2021.server.model.entity;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "TaskWorkLogs")
public class TaskWorkLog {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    private Task task;

    @ManyToOne(optional = false)
    private User principal;

    @Column(nullable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime ts_start;

    // TODO(@Roos): Add constraint ts_end > ts_start.
    @Column(nullable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime ts_end;
}
