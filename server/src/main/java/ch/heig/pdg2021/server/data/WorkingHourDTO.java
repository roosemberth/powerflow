package ch.heig.pdg2021.server.data;

import lombok.Data;

import java.time.LocalTime;

@Data
public class WorkingHourDTO {
    enum Day {
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday;
    }

    private Day day;

    private LocalTime ts_start;

    private LocalTime ts_end;
}
