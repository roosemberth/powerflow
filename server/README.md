# Server for powerflow

If you're looking for the design documentation, please take a look at [the
dedicated document](./docs/Design.md) on the docs folder.

## Producing a JAR

The following command can be used to produce a standalone JAR.

```console
mvn clean compile package -Dmaven.test.skip=true
```

## Running the server

This section describes the bare minimum required to operate the server.
It is highly discouraged for production or production-like (e.g. staging)
deployments.

### Prerequisites

Running the server requires a PostgreSQL database.

### Executing the application

The following command may be used to start the server:

```console
java -jar target/server-*.jar \
    --db.url=<dbhost>:<dbport>/<database>\
    --db.user=<username> \
    --db.pass=<password>
```

Example

```console
java -jar target/server-*.jar \
    --db.url=localhost:5432/powerflow \
    --db.user=someuser \
    --db.pass=placeholder-password
```

The server will automatically create the adequate database schema if it not
exists, or try to update it if it already exists.
Mind that updating the database schema is a risky operation since it may lead
to a divergence between the schema expected by the application and the one
present in the database; make sure you understand the evolution of the database
schema across different revisions of the application.

We acknowledge this is a flaky behaviour and should be properly addressed with
a real database migration strategy.
At the moment, wiping the database clean is recommended across incompatible
versions.

### Running the tests

[Spring boot][spring] will provide an [embedded database][embedded-db] for the
tests to run with.
You can also provide a PostgreSQL database (at your option, but highly
recommended), but mind that you may have to manually delete and recreate the
database and its schema upon every invocation.

The continuous integration will test in a real PostgreSQL database nonetheless.

[spring]: https://spring.io/projects/spring-boot
[embedded-db]: https://docs.spring.io/spring-session/docs/1.3.0.RELEASE/reference/html5/guides/httpsession-jdbc-boot.html#httpsession-jdbc-boot-configuration

#### Examples

Running tests using the embedded database:

```console
mvn clean test
```

##### Running with an external PostgreSQL database

DROP, CREATE database and populate database schema:

```console
sudo -u postgres psql \
    -c 'DROP DATABASE "db-for-powerflow";' \
    -c 'CREATE DATABASE "db-for-powerflow" OWNER someUser;'; \
sudo -u postgres psql db-for-powerflow someUser \
    -f ./src/main/resources/schema.sql
```

Run tests on postgresql, skipping datasource initialization:

```console
mvn clean test \
    -Dspring.datasource.url=jdbc:postgresql://localhost:5432/db-for-powerflow \
    -Dspring.datasource.username=someUser \
    -Dspring.datasource.password=somePass \
    -Dspring.datasource.initialization-mode=never
```

### Running the server during development

To ease the development cycle, the server may be ran directly from `mvn`.
It is recommended that you include the `clean` command every time you run to
avoid yourself some headaches, but you can remove it to speed up bootstrap time.

The same prerequisites as running in production apply.

Example command:

```console
mvn clean spring-boot:run -Dspring-boot.run.arguments="\
    --db.url=localhost:5432/powerflow \
    --db.user=user\
    --db.pass=pass\
    "
```

(For the eagle eye, this is effectively putting all arguments inside a single
argument to maven, hence any quotes inside must be escaped.)
