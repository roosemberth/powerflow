---
title: Infrastructure overview of the Powerflow platform
author: Roosembert Palacios
---

The current infrastructure deployment is configured for an agile development
model.

- Snapshotting and rollback is not currently supported.
- No data backups are configure since none of the deployed components have
  declared any data to store.
- Every component execution is ephemeral, in particular any component may be
  spontaneously restarted. No undeclared data is preserved across restarts.

## Deployment strategy when code is updated in the main branch

```mermaid
graph LR
  subgraph Developers
    Code
  end

  subgraph GitLab
    Repository
    CI["Continuous integration"]
    Registry["Container registry"]
    Repository -->|Trigger| CI
    CI -->|push OCI image| Registry
  end

  Code -->|Merge| Repository

  subgraph VM["Data processing server (Yverdon-les-bains, Switzerland)"]
    DeploymentHooks

    subgraph Prod["Production network"]
      ProdWeb["flutter-app container"]
    end
    subgraph Stag["Staging network"]
      StagWeb["flutter-app container"]
    end

    DeploymentHooks -->|Fetch| Registry
    DeploymentHooks -->|"Update/Run"| StagWeb
    DeploymentHooks -->|"Update/Run"| ProdWeb
  end

  CI -->|trigger| DeploymentHooks
```

## Management of the powerflow infrastructure

Server are provided by the IICT institute at [HEIG-VD](https://heig-vd.ch/).

Hosting and operations are described in [the infra subproject](../infra) in this
repository.

The domain name used by powerflow is `powerflow.orbstheorem.ch.` and
`staging.powerflow.orbstheorem.ch`.
The domain and zone are provided and maintained by _Roosembert Palacios_.
SSL certificates are provided by [Let's encrypt][le], they attest **domain
control** and authenticate the domains users are connecting to; these
certificates do not provide entity validation nor identity.

[le]: https://letsencrypt.org/
