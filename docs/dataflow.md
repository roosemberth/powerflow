---
title: Dataflow overview of the Powerflow platform
author: Roosembert Palacios
---

## Dataflow when a normal user navigates to the main webpage.

Note: The main website is currently available at
<https://powerflow.orbstheorem.ch>.

```mermaid
graph LR
  subgraph Servers["Powerflow VM (Yverdon-les-bains, Switzerland)"]
    subgraph Docker
      LB["Request routing & SSL reverse proxy (Traefik)"]
      ProdWeb["flutter-app container"]
    end
    LB -->|HTTP| ProdWeb
  end

  subgraph Clients
    Client
  end

  Client -->|HTTPS| LB
```
